## 2.2.10
- deserialize map values when specifiedType is Value

## 2.2.9
- read from the integerValue as a backup in the double serializer

## 2.2.8
- deserialize map values correctly in JsonObject serializer

## 2.2.7
- added JsonObject serializer

## 2.2.6
- avoiding calls to dynamic target

## 2.2.5
- deserialize map keys

## 2.2.4
- fixed trailing comma lint warnings

## 2.2.3
- removed UTC DateTime restriction

## 2.0.3
- updated dependencies

## 2.0.2
- fixed analysis warnings

## 2.0.1
- added const for EntityType in deserialize method
- use dynamic instead of Object when deserializing
