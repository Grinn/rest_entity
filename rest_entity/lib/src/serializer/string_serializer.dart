import 'package:googleapis/firestore/v1.dart' as firestore;
import 'package:rest_entity/src/serializer/entity_serializer.dart';
import 'package:rest_entity/src/serializer/entity_serializers.dart';
import 'package:rest_entity/src/serializer/entity_type.dart';
import 'package:rest_entity/src/serializer/rest_entity_serializers.dart';

class StringSerializer implements PrimitiveEntitySerializer<String?> {
  final bool structured = false;
  @override
  final Iterable<EntityType> types = const [EntityType(String)];
  @override
  final String wireName = 'String';

  @override
  Map serialize(EntitySerializers serializers, String? string,
      {EntityType? specifiedType}) {
    final _value = firestore.Value();
    _value.stringValue = string;
    return _value.toJson();
  }

  @override
  firestore.Value serializeValue(
      RestEntitySerializers serializers, String? object,
      {EntityType? specifiedType}) {
    final _value = firestore.Value();
    _value.stringValue = object;
    return _value;
  }

  @override
  String? deserialize(EntitySerializers serializers, Map fields,
      {EntityType? specifiedType}) {
    final _value = firestore.Value.fromJson(fields);
    return _value.stringValue;
  }

  @override
  String? deserializeValue(
      RestEntitySerializers serializers, Map<String, firestore.Value> values,
      {EntityType? specifiedType}) {
    if (values.containsKey('stringValue')) {
      final value = values['stringValue'];
      return value?.stringValue;
    } else if (values.isNotEmpty) {
      final _key = values.keys.first;
      return values[_key]?.stringValue;
    }
    return null;
  }

  @override
  String? deserializeFromJson(RestEntitySerializers serializers, String? object,
      {EntityType? specifiedType}) {
    return object;
  }
}
