import 'package:googleapis/firestore/v1.dart' as firestore;
import 'package:rest_entity/src/serializer/entity_serializer.dart';
import 'package:rest_entity/src/serializer/entity_serializers.dart';
import 'package:rest_entity/src/serializer/entity_type.dart';
import 'package:rest_entity/src/serializer/rest_entity_serializers.dart';

class BigIntSerializer implements PrimitiveEntitySerializer<BigInt?> {
  final bool structured = false;

  // [BigInt] has a private implementation type; register it via [BigInt.zero].
  @override
  final Iterable<EntityType> types = [
    const EntityType(BigInt),
    EntityType(BigInt.zero.runtimeType)
  ];

  @override
  final String wireName = 'BigInt';

  @override
  Map serialize(EntitySerializers serializers, BigInt? bigInt,
      {EntityType? specifiedType}) {
    final _value = firestore.Value();
    _value.integerValue = bigInt.toString();
    return _value.toJson();
  }

  @override
  firestore.Value serializeValue(
      RestEntitySerializers serializers, BigInt? object,
      {EntityType? specifiedType}) {
    final _value = firestore.Value();
    _value.integerValue = object?.toString();
    return _value;
  }

  @override
  BigInt? deserialize(EntitySerializers serializers, Map fields,
      {EntityType? specifiedType}) {
    final _value = firestore.Value.fromJson(fields);
    return _value.integerValue != null
        ? BigInt.parse(_value.integerValue!)
        : null;
  }

  @override
  BigInt? deserializeValue(
      RestEntitySerializers serializers, Map<String, firestore.Value> values,
      {EntityType? specifiedType}) {
    final value = values['integerValue'];
    return value?.integerValue != null
        ? BigInt.parse(value!.integerValue!)
        : null;
  }

  @override
  BigInt? deserializeFromJson(RestEntitySerializers serializers, BigInt? object,
      {EntityType? specifiedType}) {
    return object;
  }
}
