import 'package:googleapis/firestore/v1.dart' as firestore;
import 'package:rest_entity/src/serializer/entity_serializer.dart';
import 'package:rest_entity/src/serializer/entity_serializers.dart';
import 'package:rest_entity/src/serializer/entity_type.dart';
import 'package:rest_entity/src/serializer/rest_entity_serializers.dart';

class NumSerializer implements PrimitiveEntitySerializer<num?> {
  final bool structured = false;
  @override
  final Iterable<EntityType> types = const [EntityType(num)];
  @override
  final String wireName = 'num';

  @override
  Map serialize(EntitySerializers serializers, num? number,
      {EntityType? specifiedType}) {
    final _value = firestore.Value();
    _value.integerValue = number.toString();
    return _value.toJson();
  }

  @override
  firestore.Value serializeValue(RestEntitySerializers serializers, num? object,
      {EntityType? specifiedType}) {
    final _value = firestore.Value();
    _value.integerValue = object.toString();
    return _value;
  }

  @override
  num? deserialize(EntitySerializers serializers, Map fields,
      {EntityType? specifiedType}) {
    final _value = firestore.Value.fromJson(fields);
    return _value.integerValue != null
        ? int.parse(_value.integerValue!) as num
        : null;
  }

  @override
  num? deserializeValue(
      RestEntitySerializers serializers, Map<String, firestore.Value> values,
      {EntityType? specifiedType}) {
    final value = values['integerValue'];
    final _integer = value?.integerValue != null ? value!.integerValue : null;
    return int.tryParse(_integer ?? '0');
  }

  @override
  num? deserializeFromJson(RestEntitySerializers serializers, num? object,
      {EntityType? specifiedType}) {
    return object;
  }
}
