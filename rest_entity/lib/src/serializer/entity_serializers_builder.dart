import 'package:rest_entity/src/serializer/entity_type.dart';
import 'package:rest_entity/src/serializer/rest_entity_serializers.dart';
import 'package:rest_entity/src/serializer/entity_serializer.dart';

/// Builder for [RestEntitySerializers].
abstract class EntitySerializersBuilder {
  factory EntitySerializersBuilder() = RestEntitySerializersBuilder;

  /// Adds a [EntitySerializer]. It will be used to handle the type(s) it declares
  /// via its `types` property.
  void add(EntitySerializer serializer);

  /// Adds an iterable of [EntitySerializer].
  void addAll(Iterable<EntitySerializer> serializers);

  /// Adds a builder factory.
  ///
  /// Builder factories are needed when deserializing to types that use
  /// generics. For example, to deserialize a `BuiltList<Foo>`, `built_value`
  /// needs a builder factory for `BuiltList<Foo>`.
  ///
  /// `built_value` tries to generate code that will install all the builder
  /// factories you need, but this support is incomplete. So you may need to
  /// add your own. For example:
  ///
  /// ```dart
  /// serializers = (serializers.toBuilder()
  ///       ..addBuilderFactory(
  ///         const FullType(BuiltList, [FullType(Foo)]),
  ///         () => ListBuilder<Foo>(),
  ///       ))
  ///     .build();
  /// ```
  void addBuilderFactory(EntityType specifiedType, Function function);

  void addMapKeyBuilderFactory(EntityType specifiedType, Function function);

  RestEntitySerializers build();
}
