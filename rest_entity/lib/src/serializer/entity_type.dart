import 'package:googleapis/firestore/v1.dart';
import 'package:quiver/core.dart';

/// A [Type] with, optionally, [EntityType] generic type parameters.
///
/// May also be [unspecified], indicating that no type information is
/// available.
class EntityType {
  /// An unspecified type.
  static const EntityType unspecified = EntityType(null);
  static const EntityType value = EntityType(Value);

  /// The [Object] type.
  static const EntityType object = EntityType(Object);

  /// The root of the type.
  final Type? root;

  /// Type parameters of the type.
  final List<EntityType> parameters;

  const EntityType(this.root, [this.parameters = const []]);

  bool get isUnspecified => identical(root, null);

  @override
  bool operator ==(dynamic other) {
    if (identical(other, this)) return true;
    if (other is! EntityType) return false;
    if (root != other.root) return false;
    if (parameters.length != other.parameters.length) return false;
    for (var i = 0; i != parameters.length; ++i) {
      if (parameters[i] != other.parameters[i]) return false;
    }
    return true;
  }

  @override
  int get hashCode {
    return hash2(root, hashObjects(parameters));
  }

  @override
  String toString() => isUnspecified
      ? 'unspecified'
      : parameters.isEmpty
          ? _getRawName(root)
          : '${_getRawName(root)}<${parameters.join(", ")}>';

  static String _getRawName(Type? type) {
    final name = type.toString();
    final genericsStart = name.indexOf('<');
    return genericsStart == -1 ? name : name.substring(0, genericsStart);
  }
}
