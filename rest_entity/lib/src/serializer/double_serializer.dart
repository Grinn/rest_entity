import 'package:googleapis/firestore/v1.dart' as firestore;
import 'package:rest_entity/src/serializer/entity_serializer.dart';
import 'package:rest_entity/src/serializer/entity_serializers.dart';
import 'package:rest_entity/src/serializer/entity_type.dart';
import 'package:rest_entity/src/serializer/rest_entity_serializers.dart';

class DoubleSerializer implements PrimitiveEntitySerializer<double?> {
  // Constant names match those in [double].
  // ignore_for_file: non_constant_identifier_names
  static const String nan = 'NaN';
  static const String infinity = 'INF';
  static const String negativeInfinity = '-INF';

  final bool structured = false;
  @override
  final Iterable<EntityType> types = const [EntityType(double)];
  @override
  final String wireName = 'double';

  @override
  Map serialize(EntitySerializers serializers, double? aDouble,
      {EntityType? specifiedType}) {
    final _value = firestore.Value();
    _value.doubleValue = aDouble;
    return _value.toJson();
  }

  @override
  firestore.Value serializeValue(
      RestEntitySerializers serializers, double? object,
      {EntityType? specifiedType}) {
    final _value = firestore.Value();
    _value.doubleValue = object;
    return _value;
  }

  @override
  double? deserialize(EntitySerializers serializers, Map fields,
      {EntityType? specifiedType}) {
    final _value = firestore.Value.fromJson(fields);
    return _value.doubleValue ?? double.tryParse(_value.integerValue!);
  }

  @override
  double? deserializeValue(
      RestEntitySerializers serializers, Map<String, firestore.Value> values,
      {EntityType? specifiedType}) {
    final doubleValue = values['doubleValue'];
    final _double = doubleValue?.doubleValue;
    final integerValue = values['integerValue'];
    final _int = int.tryParse(integerValue?.integerValue ?? '');
    return _double ?? _int?.toDouble() ?? 0.0;
  }

  @override
  double? deserializeFromJson(RestEntitySerializers serializers, double? object,
      {EntityType? specifiedType}) {
    return object;
  }
}
