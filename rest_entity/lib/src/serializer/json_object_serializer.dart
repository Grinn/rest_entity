import 'dart:convert';

import 'package:built_value/json_object.dart';
import 'package:googleapis/firestore/v1.dart' as firestore;
import 'package:rest_entity/src/serializer/entity_serializer.dart';
import 'package:rest_entity/src/serializer/entity_serializers.dart';
import 'package:rest_entity/src/serializer/entity_type.dart';
import 'package:rest_entity/src/serializer/rest_entity_serializers.dart';

class JsonObjectSerializer implements PrimitiveEntitySerializer<JsonObject?> {
  final bool structured = false;
  @override
  final Iterable<EntityType> types = const [EntityType(JsonObject)];
  @override
  final String wireName = 'JsonObject';

  @override
  Map serialize(EntitySerializers serializers, JsonObject? object,
      {EntityType? specifiedType}) {
    final _value = _serializeToValue(object, serializers);
    return _value.toJson();
  }

  firestore.Value _serializeToValue(
      JsonObject? object, EntitySerializers serializers) {
    final _value = firestore.Value();
    if (object != null) {
      if (object.isBool) {
        _value.booleanValue = object.asBool;
      }
      if (object.isNum) {
        if (object.asNum is int) {
          _value.integerValue = object.asNum.toString();
        } else if (object.asNum is double) {
          _value.doubleValue = double.parse(object.asNum.toString());
        }
      }
      if (object.isString) {
        _value.stringValue = object.asString;
      }

      if (object.isList) {
        final _arrayValue = firestore.ArrayValue();
        _arrayValue.values = object.asList.map<firestore.Value>(
          (item) {
            firestore.Value? _entryValue;
            try {
              if (item is Map<String, dynamic>) {
                _entryValue = serializers.serializeValue(JsonObject(item),
                    specifiedType: const EntityType(JsonObject));
              } else {
                _entryValue = firestore.Value.fromJson(
                  serializers.serialize(item as Object,
                      specifiedType: EntityType.value) as Map<String, dynamic>,
                );
              }
            } catch (e) {
              _entryValue =
                  firestore.Value.fromJson(item as Map<String, dynamic>);
            }
            return _entryValue;
          },
        ).toList();
        _value.arrayValue = _arrayValue;
      }

      if (object.isMap) {
        final _mapValue = firestore.MapValue();
        _mapValue.fields =
            object.asMap.map<String, firestore.Value>((key, value) {
          if (isMapValue(value)) {
            final _serializedValue = serializers.serializeValue(
                JsonObject(value),
                specifiedType: const EntityType(JsonObject));
            return MapEntry(key.toString(), _serializedValue);
          } else {
            final _entryValue = firestore.Value.fromJson(serializers.serialize(
                    value as Object,
                    specifiedType: EntityType(value.runtimeType))
                as Map<String, dynamic>);
            return MapEntry(key.toString(), _entryValue);
          }
        });
        _value.mapValue = _mapValue;
      }
    }
    return _value;
  }

  bool isMapValue(dynamic value) =>
      value.runtimeType.toString().startsWith('_InternalLinkedHashMap') ||
      value.runtimeType.toString().startsWith('_Map');

  @override
  firestore.Value serializeValue(
      RestEntitySerializers serializers, JsonObject? object,
      {EntityType? specifiedType}) {
    return _serializeToValue(object, serializers);
  }

  @override
  JsonObject? deserialize(EntitySerializers serializers, Map fields,
      {EntityType? specifiedType}) {
    final _value = firestore.Value.fromJson(fields);
    return _deserializeValue(_value, serializers);
  }

  JsonObject? _deserializeValue(
      firestore.Value _value, EntitySerializers serializers) {
    JsonObject? _object;
    if (_value.stringValue != null) {
      _object = JsonObject(_value.stringValue);
    }
    if (_value.integerValue != null) {
      _object = JsonObject(int.parse(_value.integerValue!));
    }
    if (_value.doubleValue != null) {
      _object = JsonObject(_value.doubleValue);
    }
    if (_value.booleanValue != null) {
      _object = JsonObject(_value.booleanValue);
    }
    if (_value.arrayValue != null) {
      final _values = _value.arrayValue?.values?.map((value) {
            final _asMap = value.toJson();
            final _keyName = _asMap.keys.first;
            return serializers.deserializeValue({_keyName: value},
                specifiedType: EntityType.value);
          }).toList() ??
          [];
      _object = JsonObject(_values);
    }
    if (_value.mapValue != null) {
      final _entries = _value.mapValue?.fields?.map((key, value) {
            late dynamic _entryKey;
            Map? _keyAsJson;
            try {
              _keyAsJson = jsonDecode(key) as Map?;
            } catch (e) {
              //NOOP - use key as string directly
            }
            if (_keyAsJson == null) {
              _entryKey = key;
            } else {
              _entryKey = serializers.deserializeFromJson(
                  _keyAsJson, EntityType.unspecified);
            }
            final _entryValue = serializers.deserializeValue({key: value},
                specifiedType: EntityType.value);
            return MapEntry(_entryKey, _entryValue);
          }) ??
          {};
      _object = JsonObject(_entries);
    }
    return _object;
  }

  @override
  JsonObject? deserializeValue(
      RestEntitySerializers serializers, Map<String, firestore.Value> values,
      {EntityType? specifiedType}) {
    if (values.length == 1) {
      return _deserializeValue(values.values.first, serializers);
    }
    if (values['arrayValue'] != null) {
      return _deserializeValue(values['arrayValue']!, serializers);
    }
    if (values['mapValue'] != null) {
      return _deserializeValue(values['mapValue']!, serializers);
    }
    final value = firestore.Value.fromJson(values);
    return _deserializeValue(value, serializers);
  }

  @override
  JsonObject? deserializeFromJson(
      RestEntitySerializers serializers, JsonObject? object,
      {EntityType? specifiedType}) {
    return object;
  }
}
