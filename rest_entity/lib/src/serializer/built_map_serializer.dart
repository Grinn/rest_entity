import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:googleapis/firestore/v1.dart' as firestore;
import 'package:rest_entity/src/serializer/entity_serializer.dart';
import 'package:rest_entity/src/serializer/entity_serializers.dart';
import 'package:rest_entity/src/serializer/entity_type.dart';
import 'package:rest_entity/src/serializer/rest_entity_serializers.dart';

class BuiltMapSerializer implements StructuredEntitySerializer<BuiltMap> {
  final bool structured = true;
  @override
  final Iterable<EntityType> types = const [EntityType(BuiltMap)];
  @override
  final String wireName = 'map';

  @override
  Map serialize(EntitySerializers serializers, BuiltMap builtMap,
      {EntityType? specifiedType = EntityType.unspecified}) {
    final isUnderspecified =
        specifiedType!.isUnspecified || specifiedType.parameters.isEmpty;
    if (!isUnderspecified) serializers.expectBuilder(specifiedType);

    final valueType = specifiedType.parameters.isEmpty
        ? EntityType.unspecified
        : specifiedType.parameters[1];

    final _value = firestore.Value();
    final _mapValue = firestore.MapValue();
    _mapValue.fields = builtMap.map((key, value) {
      final _entryValue = firestore.Value.fromJson(
          serializers.serialize(value as Object, specifiedType: valueType)
              as Map<String, dynamic>);
      return MapEntry(key.toString(), _entryValue);
    }).toMap();
    _value.mapValue = _mapValue;
    return _value.toJson();
  }

  @override
  firestore.Value serializeValue(
      RestEntitySerializers serializers, BuiltMap builtMap,
      {EntityType? specifiedType}) {
    final isUnderspecified =
        specifiedType!.isUnspecified || specifiedType.parameters.isEmpty;
    if (!isUnderspecified) serializers.expectBuilder(specifiedType);

    final keyType = specifiedType.parameters.isEmpty
        ? EntityType.unspecified
        : specifiedType.parameters[0];

    final valueType = specifiedType.parameters.isEmpty
        ? EntityType.unspecified
        : specifiedType.parameters[1];

    final _value = firestore.Value();
    final _mapValue = firestore.MapValue();
    _mapValue.fields = builtMap.map((key, value) {
      late String _keyValue;
      if (key is String) {
        _keyValue = key;
      } else {
        _keyValue =
            jsonEncode(serializers.serialize(key, specifiedType: keyType));
      }
      final _entryValue =
          serializers.serializeValue(value as Object, specifiedType: valueType);
      return MapEntry(_keyValue, _entryValue);
    }).toMap();
    _value.mapValue = _mapValue;
    return _value;
  }

  @override
  BuiltMap deserialize(EntitySerializers serializers, Map fields,
      {EntityType? specifiedType = EntityType.unspecified}) {
    final isUnderspecified =
        specifiedType!.isUnspecified || specifiedType.parameters.isEmpty;

    final keyType = specifiedType.parameters.isEmpty
        ? EntityType.unspecified
        : specifiedType.parameters[0];
    final valueType = specifiedType.parameters.isEmpty
        ? EntityType.unspecified
        : specifiedType.parameters[1];

    final result = isUnderspecified
        ? MapBuilder<Object, Object>()
        : (serializers.newBuilder(specifiedType) as MapBuilder?)!;
    final _value = firestore.Value.fromJson(fields);
    final _entries = _value.mapValue?.fields?.map((key, value) {
          final _entryKey =
              serializers.deserializeFromJson(jsonDecode(key), keyType);

          final _entryValue =
              serializers.deserialize(value.toJson(), specifiedType: valueType);
          return MapEntry(_entryKey, _entryValue);
        }) ??
        {};
    result.replace(_entries);
    return result.build();
  }

  @override
  BuiltMap deserializeValue(
      RestEntitySerializers serializers, Map<String, firestore.Value> values,
      {EntityType? specifiedType}) {
    final isUnderspecified =
        specifiedType!.isUnspecified || specifiedType.parameters.isEmpty;

    final keyType = specifiedType.parameters.isEmpty
        ? EntityType.unspecified
        : specifiedType.parameters[0];
    final valueType = specifiedType.parameters.isEmpty
        ? EntityType.unspecified
        : specifiedType.parameters[1];

    final result = isUnderspecified
        ? MapBuilder<Object, Object>()
        : (serializers.newBuilder(specifiedType) as MapBuilder?)!;
    final value = values['mapValue'];
    final _entries = value?.mapValue?.fields?.map((key, value) {
          final _entryValue = serializers
              .deserializeValue({key: value}, specifiedType: valueType);
          return MapEntry(key, _entryValue);
        }) ??
        {};
    result.replace(_entries);
    return result.build();
  }

  @override
  BuiltMap deserializeFromJson(RestEntitySerializers serializers, Map? fields,
      {EntityType? specifiedType}) {
    // TODO: implement deserializeFromJson
    throw UnimplementedError();
  }
}
