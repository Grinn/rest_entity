import 'package:built_collection/built_collection.dart';
import 'package:googleapis/firestore/v1.dart' as firestore;
import 'package:rest_entity/src/serializer/big_int_serializer.dart';
import 'package:rest_entity/src/serializer/bool_serializer.dart';
import 'package:rest_entity/src/serializer/built_list_serializer.dart';
import 'package:rest_entity/src/serializer/built_map_serializer.dart';
import 'package:rest_entity/src/serializer/date_time_serializer.dart';
import 'package:rest_entity/src/serializer/double_serializer.dart';
import 'package:rest_entity/src/serializer/duration_serializer.dart';
import 'package:rest_entity/src/serializer/entity_serializer.dart';
import 'package:rest_entity/src/serializer/entity_serializers_builder.dart';
import 'package:rest_entity/src/serializer/entity_type.dart';
import 'package:rest_entity/src/serializer/int_serializer.dart';
import 'package:rest_entity/src/serializer/json_object_serializer.dart';
import 'package:rest_entity/src/serializer/num_serializer.dart';
import 'package:rest_entity/src/serializer/string_serializer.dart';

class EntitySerializersFor {
  final List<Type> types;

  const EntitySerializersFor(this.types);
}

abstract class EntitySerializers {
  /// Default [EntitySerializers] that can serialize primitives and collections.
  ///
  /// Use [toBuilder] to add more serializers.
  factory EntitySerializers() {
    return (EntitySerializersBuilder()
          ..add(BuiltListSerializer())
          ..add(BuiltMapSerializer())
          ..add(BigIntSerializer())
          ..add(BoolSerializer())
          ..add(DateTimeSerializer())
          ..add(DoubleSerializer())
          ..add(DurationSerializer())
          ..add(IntSerializer())
          ..add(NumSerializer())
          ..add(StringSerializer())
          ..add(JsonObjectSerializer())
          ..addBuilderFactory(const EntityType(BuiltList, [EntityType(String)]),
              () => ListBuilder<String>())
          ..addBuilderFactory(const EntityType(BuiltList, [EntityType(double)]),
              () => ListBuilder<double>())
          ..addBuilderFactory(const EntityType(BuiltList, [EntityType(num)]),
              () => ListBuilder<num>())
          ..addBuilderFactory(const EntityType(BuiltList, [EntityType(int)]),
              () => ListBuilder<int>()))
        .build();
  }

  /// The installed [EntitySerializer]s.
  Iterable<EntitySerializer> get serializers;

  /// Serializes [object].
  ///
  /// A [EntitySerializer] must have been provided for every type the object uses.
  ///
  /// Types that are known statically can be provided via [specifiedType]. This
  /// will reduce the amount of data needed on the wire. The exact same
  /// [specifiedType] will be needed to deserialize.
  ///
  /// Create one using [EntitySerializersBuilder].
  ///
  Object serialize(Object object, {EntityType? specifiedType});

  /// Convenience method for when you know the type you're serializing.
  /// Specify the type by specifying its [EntitySerializer] class. Equivalent to
  /// calling [serialize] with a `specifiedType`.
  Object serializeWith<T>(EntitySerializer<T> entitySerializer, T object);

  /// Convenience method for when you know the type you're serializing.
  /// Specify the type by specifying its [EntitySerializer] class. Equivalent to
  /// calling [serialize] with a `specifiedType`.
  firestore.Value serializeValueWith<T>(
      EntitySerializer<T> entitySerializer, T object);

  /// Serializes [object].
  ///
  /// A [EntitySerializer] must have been provided for every type the object uses.
  ///
  /// Types that are known statically can be provided via [specifiedType]. This
  /// will reduce the amount of data needed on the wire. The exact same
  /// [specifiedType] will be needed to deserialize.
  ///
  /// Create one using [EntitySerializersBuilder].
  ///
  firestore.Value serializeValue(Object? object, {EntityType? specifiedType});

  /// Deserializes [serialized].
  ///
  /// A [EntitySerializer] must have been provided for every type the object uses.
  ///
  /// If [serialized] was produced by calling [serialize] with [specifiedType],
  /// the exact same [specifiedType] must be provided to deserialize.
  dynamic deserialize(dynamic serialized, {EntityType? specifiedType});

  /// Deserializes [serialized] from a Firestore Value object.
  ///
  /// A [EntitySerializer] must have been provided for every type the object uses.
  ///
  /// return the jsonDecoded variant of this object's String.
  dynamic deserializeValue(Map<String, firestore.Value> object,
      {EntityType? specifiedType = EntityType.unspecified});

  /// Deserializes [serialized] from Json.
  ///
  /// A [EntitySerializer] must have been provided for every type the object uses.
  ///
  /// return the jsonDecoded variant of this object's String.
  dynamic deserializeFromJson(Object? serialized, EntityType specifiedType);

  /// Convenience method for when you know the type you're deserializing.
  /// Specify the type by specifying its [EntitySerializer] class. Equivalent to
  /// calling [deserialize] with a `specifiedType`.
  T deserializeWith<T>(EntitySerializer<T> entitySerializer, Object fields);

  /// Convenience method for when you know the type you're deserializing.
  /// Specify the type by specifying its [EntitySerializer] class. Equivalent to
  /// calling [deserialize] with a `specifiedType`.
  T deserializeValueWith<T>(EntitySerializer<T> entitySerializer,
      Map<String, firestore.Value> fields);

  /// Gets a serializer; returns `null` if none is found. For use in plugins
  /// and other extension code.
  EntitySerializer? serializerForType(EntityType type);

  /// Gets a serializer; returns `null` if none is found. For use in plugins
  /// and other extension code.
  EntitySerializer? serializerForWireName(String wireName);

  /// Creates a new builder for the type represented by [fullType].
  ///
  /// For example, if [fullType] is `BuiltList<int, String>`, returns a
  /// `ListBuilder<int, String>`. This helps serializers to instantiate with
  /// correct generic type parameters.
  ///
  /// Throws a [StateError] if no matching builder factory has been added.
  Object? newBuilder(EntityType? fullType);

  Function? newMapKeyBuilder(EntityType fullType);

  /// Whether a builder for [fullType] is available via [newBuilder].
  bool hasBuilder(EntityType fullType);

  /// Throws if a builder for [fullType] is not available via [newBuilder].
  void expectBuilder(EntityType? fullType);

  /// The installed builder factories.
  Map<EntityType, Function> get builderFactories;

  /// The installed builder factories for map keys.
  Map<EntityType, Function> get mapKeyBuilderFactories;

  EntitySerializersBuilder toBuilder();
}
