// Copyright (c) 2015, Google Inc. Please see the AUTHORS file for details.

import 'package:googleapis/firestore/v1.dart' as firestore;
import 'package:rest_entity/src/serializer/deserialization_exception.dart';
import 'package:rest_entity/src/serializer/entity_serializer.dart';
import 'package:rest_entity/src/serializer/entity_serializers.dart';
import 'package:rest_entity/src/serializer/entity_serializers_builder.dart';
import 'package:rest_entity/src/serializer/entity_type.dart';

/// Default implementation of [Serializers].
class RestEntitySerializers implements EntitySerializers {
  final Map<EntityType, EntitySerializer> _typeToSerializer;

  // Implementation note: wire name is what gets sent in the JSON, type name is
  // the runtime type name. Type name is complicated for two reasons:
  //
  // 1. Built Value classes have two types, the abstract class and the
  // generated implementation.
  //
  // 2. When compiled to javascript the runtime type names are obfuscated.
  final Map<String, EntitySerializer> _wireNameToSerializer;
  final Map<String, EntitySerializer> _typeNameToSerializer;

  @override
  final Map<EntityType, Function> builderFactories;

  @override
  final Map<EntityType, Function> mapKeyBuilderFactories;

  RestEntitySerializers._(
      this._typeToSerializer,
      this._wireNameToSerializer,
      this._typeNameToSerializer,
      this.builderFactories,
      this.mapKeyBuilderFactories);

  @override
  Iterable<EntitySerializer> get serializers => _wireNameToSerializer.values;

  @override
  T deserializeWith<T>(EntitySerializer<T> serializer, dynamic serialized) {
    return deserialize(serialized, specifiedType: serializer.types.first) as T;
  }

  @override
  T deserializeValueWith<T>(EntitySerializer<T> entitySerializer,
      Map<String, firestore.Value> value) {
    return deserializeValue(value, specifiedType: entitySerializer.types.first)
        as T;
  }

  @override
  Object serializeWith<T>(EntitySerializer<T> serializer, T object) {
    return serialize(object, specifiedType: serializer.types.first);
  }

  @override
  firestore.Value serializeValueWith<T>(
      EntitySerializer<T> entitySerializer, T object) {
    return serializeValue(object, specifiedType: entitySerializer.types.first);
  }

  @override
  firestore.Value serializeValue(Object? object, {EntityType? specifiedType}) {
    if (specifiedType != null) {
      final serializer = serializerForType(specifiedType);
      if (serializer == null) {
        throw StateError("No serializer for '${object.runtimeType}'.");
      }
      if (serializer is StructuredEntitySerializer) {
        return serializer.serializeValue(this, object,
            specifiedType: specifiedType);
      } else if (serializer is PrimitiveEntitySerializer) {
        return serializer.serializeValue(this, object,
            specifiedType: specifiedType);
      } else {
        throw StateError(
            'serializer must be StructuredSerializer or PrimitiveSerializer');
      }
    }
    throw StateError('missing EntityType for $object');
  }

  @override
  Object serialize(Object? object, {EntityType? specifiedType}) {
    final transformedObject = object;
    final result = _serialize(transformedObject, specifiedType: specifiedType);
    return result;
  }

  Object _serialize(Object? object, {EntityType? specifiedType}) {
    if (specifiedType == null) {
      final serializer = serializerForType(EntityType(object.runtimeType));
      if (serializer == null) {
        throw StateError("No serializer for '${object.runtimeType}'.");
      }
      if (serializer is StructuredEntitySerializer) {
        return serializer.serialize(this, object);
      } else if (serializer is PrimitiveEntitySerializer) {
        return <Object>[
          serializer.wireName,
          serializer.serialize(this, object)
        ];
      } else {
        throw StateError(
            'serializer must be StructuredSerializer or PrimitiveSerializer');
      }
    } else {
      final serializer = serializerForType(specifiedType);
      if (serializer == null) {
        // Might be an interface; try resolving using the runtime type.
        return serialize(object);
      }
      if (serializer is StructuredEntitySerializer) {
        return serializer.serialize(this, object, specifiedType: specifiedType);
      } else if (serializer is PrimitiveEntitySerializer) {
        return serializer.serialize(this, object, specifiedType: specifiedType);
      } else {
        throw StateError(
            'serializer must be StructuredSerializer or PrimitiveSerializer');
      }
    }
  }

  @override
  dynamic deserialize(dynamic object,
      {EntityType? specifiedType = EntityType.unspecified}) {
    final transformedObject = object;
    final result = _deserialize(object, transformedObject, specifiedType);
    return result;
  }

  dynamic _deserialize(
      dynamic objectBeforePlugins, dynamic object, EntityType? specifiedType) {
    if (specifiedType == null) {
      final wireName = (object as List).first as String;

      final serializer = serializerForWireName(wireName);
      if (serializer == null) {
        throw StateError("No serializer for '$wireName'.");
      }

      if (serializer is StructuredEntitySerializer) {
        try {
          return serializer.deserialize(this, object as Map) as Map;
        } catch (error) {
          throw DeserializationException(object, specifiedType, error);
        }
      } else if (serializer is PrimitiveEntitySerializer) {
        try {
          return serializer.deserialize(this, object[1] as Map) as Map;
        } catch (error) {
          throw DeserializationException(object, specifiedType, error);
        }
      } else {
        throw StateError(
            'serializer must be StructuredSerializer or PrimitiveSerializer');
      }
    } else {
      final serializer = serializerForType(specifiedType);
      if (serializer == null) {
        if (object is List && object.first is String) {
          // Might be an interface; try resolving using the type on the wire.
          return deserialize(objectBeforePlugins);
        } else {
          throw StateError("No serializer for '$specifiedType'.");
        }
      }

      if (serializer is StructuredEntitySerializer) {
        try {
          return serializer.deserialize(this, object as Map,
              specifiedType: specifiedType) as Map;
        } catch (error) {
          throw DeserializationException(object, specifiedType, error);
        }
      } else if (serializer is PrimitiveEntitySerializer) {
        try {
          return serializer.deserialize(this, object as Map,
              specifiedType: specifiedType) as Map;
        } catch (error) {
          throw DeserializationException(object, specifiedType, error);
        }
      } else {
        throw StateError(
            'serializer must be StructuredSerializer or PrimitiveSerializer');
      }
    }
  }

  @override
  dynamic deserializeValue(Map<String, firestore.Value> object,
      {EntityType? specifiedType = EntityType.unspecified}) {
    if (specifiedType != null) {
      if (EntityType.value == specifiedType) {
        final _value = object.values.first;
        if (_value.stringValue != null) {
          return _value.stringValue;
        }
        if (_value.integerValue != null) {
          return int.parse(_value.integerValue!);
        }
        if (_value.doubleValue != null) {
          return _value.doubleValue;
        }
        if (_value.booleanValue != null) {
          return _value.booleanValue;
        }
        if (_value.arrayValue != null) {
          return _value.arrayValue?.values?.map((value) {
            return deserializeValue({'': value},
                specifiedType: EntityType.value);
          }).toList();
        }
        if (_value.mapValue != null) {
          return _value.mapValue?.fields?.map((key, value) {
            return MapEntry(
                key,
                deserializeValue({key: value},
                    specifiedType: EntityType.value));
          });
        }
      }
      final serializer = serializerForType(specifiedType);
      if (serializer == null) {
        throw StateError("No serializer for '$specifiedType'.");
      }

      if (serializer is StructuredEntitySerializer) {
        try {
          return serializer.deserializeValue(this, object,
              specifiedType: specifiedType);
        } catch (error, stacktrace) {
          print(stacktrace.toString());
          print('\n\n');
          throw DeserializationException(object, specifiedType, error);
        }
      } else if (serializer is PrimitiveEntitySerializer) {
        try {
          return serializer.deserializeValue(this, object,
              specifiedType: specifiedType);
        } catch (error, stacktrace) {
          print(stacktrace.toString());
          print('\n\n');
          throw DeserializationException(object, specifiedType, error);
        }
      } else {
        throw StateError(
            'serializer must be StructuredSerializer or PrimitiveSerializer');
      }
    }
  }

  @override
  dynamic deserializeFromJson(dynamic object, EntityType specifiedType) {
    final serializer = serializerForType(specifiedType);
    if (serializer == null) {
      if (object is List && object.first is String) {
        // Might be an interface; try resolving using the type on the wire.
        return deserialize(object);
      } else {
        throw StateError("No serializer for '$specifiedType'.");
      }
    }

    if (serializer is StructuredEntitySerializer) {
      try {
        return serializer.deserializeFromJson(this, object as Map?,
            specifiedType: specifiedType);
      } catch (error) {
        throw DeserializationException(object, specifiedType, error);
      }
    } else if (serializer is PrimitiveEntitySerializer) {
      try {
        return serializer.deserializeFromJson(this, object,
            specifiedType: specifiedType);
      } catch (error) {
        throw DeserializationException(object, specifiedType, error);
      }
    } else {
      throw StateError(
          'serializer must be StructuredSerializer or PrimitiveSerializer');
    }
  }

  @override
  EntitySerializer? serializerForType(EntityType type) =>
      _typeToSerializer[type] ?? _typeNameToSerializer[_getRawName(type)];

  @override
  EntitySerializer? serializerForWireName(String wireName) =>
      _wireNameToSerializer[wireName];

  @override
  EntitySerializersBuilder toBuilder() {
    return RestEntitySerializersBuilder._(
        _typeToSerializer,
        _wireNameToSerializer,
        _typeNameToSerializer,
        builderFactories,
        mapKeyBuilderFactories);
  }

  @override
  Object? newBuilder(EntityType? fullType) {
    final builderFactory = builderFactories[fullType!]!;
    return builderFactory();
  }

  @override
  Function? newMapKeyBuilder(EntityType fullType) {
    final builderFactory = mapKeyBuilderFactories[fullType];
    if (builderFactory == null) _throwMissingBuilderFactory(fullType);
    return builderFactory;
  }

  @override
  void expectBuilder(EntityType? fullType) {
    if (!hasBuilder(fullType)) _throwMissingBuilderFactory(fullType);
  }

  void _throwMissingBuilderFactory(EntityType? entityType) {
    throw StateError('No builder factory for $entityType. '
        'Fix by adding one, see SerializersBuilder.addBuilderFactory.');
  }

  @override
  bool hasBuilder(EntityType? fullType) {
    return builderFactories.containsKey(fullType);
  }
}

/// Default implementation of [EntitySerializersBuilder].
class RestEntitySerializersBuilder implements EntitySerializersBuilder {
  final Map<EntityType, EntitySerializer> _typeToSerializer;
  final Map<String, EntitySerializer> _wireNameToSerializer;
  final Map<String, EntitySerializer> _typeNameToSerializer;

  final Map<EntityType, Function> _builderFactories;
  final Map<EntityType, Function> _mapKeyBuilderFactories;

  factory RestEntitySerializersBuilder() => RestEntitySerializersBuilder._(
        {},
        {},
        {},
        {},
        {},
      );

  RestEntitySerializersBuilder._(
    this._typeToSerializer,
    this._wireNameToSerializer,
    this._typeNameToSerializer,
    this._builderFactories,
    this._mapKeyBuilderFactories,
  );

  @override
  void add(EntitySerializer serializer) {
    if (serializer is! StructuredEntitySerializer &&
        serializer is! PrimitiveEntitySerializer) {
      throw ArgumentError(
          'serializer must be StructuredSerializer or PrimitiveSerializer');
    }

    _wireNameToSerializer[serializer.wireName] = serializer;
    for (final type in serializer.types) {
      _typeToSerializer[type] = serializer;
      _typeNameToSerializer[_getRawName(type)] = serializer;
    }
  }

  @override
  void addAll(Iterable<EntitySerializer> serializers) {
    serializers.forEach(add);
  }

  @override
  void addBuilderFactory(EntityType types, Function function) {
    _builderFactories[types] = function;
  }

  @override
  void addMapKeyBuilderFactory(EntityType specifiedType, Function function) {
    _mapKeyBuilderFactories[specifiedType] = function;
  }

  @override
  RestEntitySerializers build() {
    return RestEntitySerializers._(
      _typeToSerializer,
      _wireNameToSerializer,
      _typeNameToSerializer,
      _builderFactories,
      _mapKeyBuilderFactories,
    );
  }
}

String _getRawName(EntityType type) {
  final name = type.toString();
  final genericsStart = name.indexOf('<');
  return genericsStart == -1 ? name : name.substring(0, genericsStart);
}
