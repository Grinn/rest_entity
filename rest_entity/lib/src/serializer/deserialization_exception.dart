import 'package:rest_entity/src/serializer/entity_type.dart';

/// [Exception] conveying why deserialization failed.
class DeserializationException implements Exception {
  final String json;
  final EntityType? type;
  final dynamic error;

  factory DeserializationException(
      Object? json, EntityType? type, dynamic error) {
    var limitedJson = json.toString();
    if (limitedJson.length > 200) {
      limitedJson =
          limitedJson.replaceRange(200, limitedJson.length, '...\n\n');
    }
    return DeserializationException._(limitedJson, type, error);
  }

  DeserializationException._(this.json, this.type, this.error);

  @override
  String toString() => "Deserializing '$json' to '$type' failed due to: $error";
}
