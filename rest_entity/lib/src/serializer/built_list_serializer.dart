import 'package:built_collection/built_collection.dart';
import 'package:googleapis/firestore/v1.dart' as firestore;
import 'package:rest_entity/src/serializer/entity_serializer.dart';
import 'package:rest_entity/src/serializer/entity_serializers.dart';
import 'package:rest_entity/src/serializer/entity_type.dart';
import 'package:rest_entity/src/serializer/rest_entity_serializers.dart';

class BuiltListSerializer implements StructuredEntitySerializer<BuiltList> {
  final bool structured = true;
  @override
  final Iterable<EntityType> types = const [EntityType(BuiltList)];
  @override
  final String wireName = 'list';

  @override
  Map serialize(EntitySerializers serializers, BuiltList builtList,
      {EntityType? specifiedType = EntityType.unspecified}) {
    final isUnderspecified =
        specifiedType!.isUnspecified || specifiedType.parameters.isEmpty;
    if (!isUnderspecified) serializers.expectBuilder(specifiedType);

    final elementType = specifiedType.parameters.isEmpty
        ? EntityType.unspecified
        : specifiedType.parameters[0];

    final _value = firestore.Value();
    final _arrayValue = firestore.ArrayValue();
    _arrayValue.values = builtList.map<firestore.Value>(
      (item) {
        final _entryValue = firestore.Value.fromJson(
          serializers.serialize(item as Object, specifiedType: elementType)
              as Map<String, dynamic>,
        );
        return _entryValue;
      },
    ).toList();
    _value.arrayValue = _arrayValue;
    return _value.toJson();
  }

  @override
  firestore.Value serializeValue(
      RestEntitySerializers serializers, BuiltList builtList,
      {EntityType? specifiedType}) {
    final isUnderspecified =
        specifiedType!.isUnspecified || specifiedType.parameters.isEmpty;
    if (!isUnderspecified) serializers.expectBuilder(specifiedType);

    final elementType = specifiedType.parameters.isEmpty
        ? EntityType.unspecified
        : specifiedType.parameters[0];

    final _value = firestore.Value();
    final _arrayValue = firestore.ArrayValue();
    _arrayValue.values = builtList.map<firestore.Value>(
      (item) {
        return serializers.serializeValue(item as Object,
            specifiedType: elementType);
      },
    ).toList();
    _value.arrayValue = _arrayValue;
    return _value;
  }

  @override
  BuiltList<Object> deserialize(EntitySerializers serializers, Map fields,
      {EntityType? specifiedType = EntityType.unspecified}) {
    final isUnderspecified =
        specifiedType!.isUnspecified || specifiedType.parameters.isEmpty;

    final elementType = specifiedType.parameters.isEmpty
        ? EntityType.unspecified
        : specifiedType.parameters[0];

    final result = isUnderspecified
        ? ListBuilder<Object>()
        : (serializers.newBuilder(specifiedType) as ListBuilder?)!;

    final _value = firestore.Value.fromJson(fields);
    final _values = _value.arrayValue?.values
            ?.map((value) => serializers.deserialize(value.toJson(),
                specifiedType: elementType))
            .toList() ??
        [];
    result.replace(_values);
    return result.build() as BuiltList<Object>;
  }

  @override
  BuiltList<Object> deserializeValue(
      RestEntitySerializers serializers, Map<String, firestore.Value> values,
      {EntityType? specifiedType}) {
    final isUnderspecified =
        specifiedType!.isUnspecified || specifiedType.parameters.isEmpty;

    final elementType = specifiedType.parameters.isEmpty
        ? EntityType.unspecified
        : specifiedType.parameters[0];

    final result = isUnderspecified
        ? ListBuilder<Object>()
        : (serializers.newBuilder(specifiedType) as ListBuilder?)!;
    final value = values['arrayValue'];
    final _values = value?.arrayValue?.values?.map((value) {
          final _asMap = value.toJson();
          final _keyName = _asMap.keys.first;
          return serializers
              .deserializeValue({_keyName: value}, specifiedType: elementType);
        }).toList() ??
        [];
    result.replace(_values);
    return result.build() as BuiltList<Object>;
  }

  @override
  BuiltList deserializeFromJson(
      RestEntitySerializers serializers, Map<dynamic, dynamic>? fields,
      {EntityType? specifiedType = EntityType.unspecified}) {
    throw UnimplementedError();
  }
}
