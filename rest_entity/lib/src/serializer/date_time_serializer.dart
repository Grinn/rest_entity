import 'package:googleapis/firestore/v1.dart' as firestore;
import 'package:rest_entity/src/serializer/entity_serializer.dart';
import 'package:rest_entity/src/serializer/entity_serializers.dart';
import 'package:rest_entity/src/serializer/entity_type.dart';
import 'package:rest_entity/src/serializer/rest_entity_serializers.dart';

class DateTimeSerializer implements PrimitiveEntitySerializer<DateTime?> {
  final bool structured = false;
  @override
  final Iterable<EntityType> types = const [EntityType(DateTime)];
  @override
  final String wireName = 'DateTime';

  @override
  Map serialize(EntitySerializers serializers, DateTime? dateTime,
      {EntityType? specifiedType}) {
    if (!dateTime!.isUtc) {
      throw ArgumentError.value(
          dateTime, 'dateTime', 'Must be in utc for serialization.');
    }
    final _value = firestore.Value();
    _value.integerValue = dateTime.microsecondsSinceEpoch.toString();
    return _value.toJson();
  }

  @override
  firestore.Value serializeValue(
      RestEntitySerializers serializers, DateTime? object,
      {EntityType? specifiedType}) {
    final _value = firestore.Value();
    _value.integerValue = object!.microsecondsSinceEpoch.toString();
    return _value;
  }

  @override
  DateTime? deserialize(EntitySerializers serializers, Map fields,
      {EntityType? specifiedType}) {
    final _value = firestore.Value.fromJson(fields);
    return _value.integerValue != null
        ? DateTime.fromMicrosecondsSinceEpoch(int.parse(_value.integerValue!),
            isUtc: true)
        : null;
  }

  @override
  DateTime? deserializeValue(
      RestEntitySerializers serializers, Map<String, firestore.Value> values,
      {EntityType? specifiedType}) {
    final value = values['integerValue'];
    final _microseconds =
        value?.integerValue != null ? value!.integerValue : null;
    return _microseconds != null
        ? DateTime.fromMicrosecondsSinceEpoch(int.parse(_microseconds),
            isUtc: true)
        : null;
  }

  @override
  DateTime? deserializeFromJson(
      RestEntitySerializers serializers, DateTime? object,
      {EntityType? specifiedType}) {
    return object;
  }
}
