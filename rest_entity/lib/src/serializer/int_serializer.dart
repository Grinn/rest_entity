import 'package:googleapis/firestore/v1.dart' as firestore;
import 'package:rest_entity/src/serializer/entity_serializer.dart';
import 'package:rest_entity/src/serializer/entity_serializers.dart';
import 'package:rest_entity/src/serializer/entity_type.dart';
import 'package:rest_entity/src/serializer/rest_entity_serializers.dart';

class IntSerializer implements PrimitiveEntitySerializer<int?> {
  final bool structured = false;
  @override
  final Iterable<EntityType> types = const [EntityType(int)];
  @override
  final String wireName = 'int';

  @override
  Map serialize(EntitySerializers serializers, int? integer,
      {EntityType? specifiedType}) {
    final _value = firestore.Value();
    _value.integerValue = integer.toString();
    return _value.toJson();
  }

  @override
  firestore.Value serializeValue(RestEntitySerializers serializers, int? object,
      {EntityType? specifiedType}) {
    final _value = firestore.Value();
    _value.integerValue = object.toString();
    return _value;
  }

  @override
  int? deserialize(EntitySerializers serializers, Map fields,
      {EntityType? specifiedType}) {
    final _value = firestore.Value.fromJson(fields);
    return _value.integerValue != null ? int.parse(_value.integerValue!) : null;
  }

  @override
  int? deserializeValue(
      RestEntitySerializers serializers, Map<String, firestore.Value> values,
      {EntityType? specifiedType}) {
    final value = values['integerValue'];
    final _integer = value?.integerValue != null ? value!.integerValue : null;
    return int.tryParse(_integer ?? '0');
  }

  @override
  int? deserializeFromJson(RestEntitySerializers serializers, int? object,
      {EntityType? specifiedType}) {
    return object;
  }
}
