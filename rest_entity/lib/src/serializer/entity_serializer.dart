import 'package:googleapis/firestore/v1.dart' as firestore;
import 'package:rest_entity/src/serializer/entity_type.dart';
import 'package:rest_entity/src/serializer/rest_entity_serializers.dart';

abstract class EntitySerializer<T> {
  /// The [Type]s that can be serialized.
  ///
  /// They must all be equal to T or a subclass of T. Subclasses are used when
  /// T is an abstract class, which is the case with built_value generated
  /// serializers.
  Iterable<EntityType> get types;

  /// The wire name of the serializable type. For most classes, the class name.
  /// For primitives and collections a lower-case name is defined as part of
  /// the `built_json` wire format.
  String get wireName;
}

/// A [EntitySerializer] that serializes to and from primitive JSON values.
abstract class PrimitiveEntitySerializer<T> implements EntitySerializer<T> {
  /// Serializes [object].
  ///
  /// Use [serializers] as needed for nested serialization. Information about
  /// the type being serialized is provided in [specifiedType].
  ///
  /// Returns a value that can be represented as a JSON primitive: a boolean,
  /// an integer, a double, or a String.
  ///
  Map serialize(RestEntitySerializers serializers, T object,
      {EntityType? specifiedType});

  /// Serializes [object].
  ///
  /// Use [serializers] as needed for nested serialization. Information about
  /// the type being serialized is provided in [specifiedType].
  ///
  /// Returns a value that can be represented as a JSON primitive: a boolean,
  /// an integer, a double, or a String.
  ///
  firestore.Value serializeValue(RestEntitySerializers serializers, T object,
      {EntityType? specifiedType});

  /// Deserializes [serialized].
  ///
  /// [serialized] is a boolean, an integer, a double or a String.
  ///
  /// Use [serializers] as needed for nested deserialization. Information about
  /// the type being deserialized is provided in [specifiedType].
  T deserialize(RestEntitySerializers serializers, Map fields,
      {EntityType? specifiedType});

  T deserializeFromJson(RestEntitySerializers serializers, T object,
      {EntityType? specifiedType});

  /// Deserializes [serialized].
  ///
  /// [serialized] is a boolean, an integer, a double or a String.
  ///
  /// Use [serializers] as needed for nested deserialization. Information about
  /// the type being deserialized is provided in [specifiedType].
  T deserializeValue(
      RestEntitySerializers serializers, Map<String, firestore.Value> value,
      {EntityType? specifiedType});
}

/// A [EntitySerializer] that serializes to and from an [Iterable] of primitive JSON
/// values.
abstract class StructuredEntitySerializer<T> implements EntitySerializer<T> {
  /// Serializes [object].
  ///
  /// Use [serializers] as needed for nested serialization. Information about
  /// the type being serialized is provided in [specifiedType].
  ///
  /// Returns an [Iterable] of values that can be represented as structured
  /// JSON: booleans, integers, doubles, Strings and [Iterable]s.
  ///
  Map serialize(RestEntitySerializers serializers, T object,
      {EntityType? specifiedType});

  /// Serializes [object].
  ///
  /// Use [serializers] as needed for nested serialization. Information about
  /// the type being serialized is provided in [specifiedType].
  ///
  /// Returns an value of values that can be represented as structured
  /// JSON: booleans, integers, doubles, Strings and [Iterable]s.
  ///
  firestore.Value serializeValue(RestEntitySerializers serializers, T object,
      {EntityType? specifiedType});

  /// Deserializes [serialized].
  ///
  /// [serialized] is an [Iterable] that may contain booleans, integers,
  /// doubles, Strings and/or [Iterable]s.
  ///
  /// Use [serializers] as needed for nested deserialization. Information about
  /// the type being deserialized is provided in [specifiedType].
  T deserialize(RestEntitySerializers serializers, Map fields,
      {EntityType? specifiedType});

  /// Deserializes [serialized].
  ///
  /// [serialized] is an [Iterable] that may contain booleans, integers,
  /// doubles, Strings and/or [Iterable]s.
  ///
  /// Use [serializers] as needed for nested deserialization. Information about
  /// the type being deserialized is provided in [specifiedType].
  T deserializeValue(
      RestEntitySerializers serializers, Map<String, firestore.Value> value,
      {EntityType? specifiedType});

  T deserializeFromJson(RestEntitySerializers serializers, Map? fields,
      {EntityType? specifiedType});
}
