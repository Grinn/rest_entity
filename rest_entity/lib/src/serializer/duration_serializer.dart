import 'package:googleapis/firestore/v1.dart' as firestore;
import 'package:rest_entity/src/serializer/entity_serializer.dart';
import 'package:rest_entity/src/serializer/entity_serializers.dart';
import 'package:rest_entity/src/serializer/entity_type.dart';
import 'package:rest_entity/src/serializer/rest_entity_serializers.dart';

class DurationSerializer implements PrimitiveEntitySerializer<Duration> {
  final bool structured = false;
  @override
  final Iterable<EntityType> types = const [EntityType(Duration)];
  @override
  final String wireName = 'Duration';

  @override
  Map serialize(EntitySerializers serializers, Duration duration,
      {EntityType? specifiedType}) {
    final _value = firestore.Value();
    _value.integerValue = duration.inMicroseconds.toString();
    return _value.toJson();
  }

  @override
  firestore.Value serializeValue(
      RestEntitySerializers serializers, Duration object,
      {EntityType? specifiedType}) {
    final _value = firestore.Value();
    _value.integerValue = object.inMicroseconds.toString();
    return _value;
  }

  @override
  Duration deserialize(EntitySerializers serializers, Map fields,
      {EntityType? specifiedType}) {
    final _value = firestore.Value.fromJson(fields);
    return Duration(microseconds: int.parse(_value.integerValue!));
  }

  @override
  Duration deserializeValue(
      RestEntitySerializers serializers, Map<String, firestore.Value> values,
      {EntityType? specifiedType}) {
    final value = values['integerValue'];
    final _microseconds =
        value?.integerValue != null ? value!.integerValue : null;
    return Duration(microseconds: int.parse(_microseconds ?? '0'));
  }

  @override
  Duration deserializeFromJson(
      RestEntitySerializers serializers, Duration object,
      {EntityType? specifiedType}) {
    return object;
  }
}
