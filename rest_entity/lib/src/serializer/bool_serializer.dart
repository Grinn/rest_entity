import 'package:googleapis/firestore/v1.dart' as firestore;
import 'package:rest_entity/src/serializer/entity_serializer.dart';
import 'package:rest_entity/src/serializer/entity_serializers.dart';
import 'package:rest_entity/src/serializer/entity_type.dart';
import 'package:rest_entity/src/serializer/rest_entity_serializers.dart';

class BoolSerializer implements PrimitiveEntitySerializer<bool?> {
  final bool structured = false;
  @override
  final Iterable<EntityType> types = const [EntityType(bool)];
  @override
  final String wireName = 'bool';

  @override
  Map serialize(EntitySerializers serializers, bool? boolean,
      {EntityType? specifiedType}) {
    final _value = firestore.Value();
    _value.booleanValue = boolean;
    return _value.toJson();
  }

  @override
  firestore.Value serializeValue(
      RestEntitySerializers serializers, bool? object,
      {EntityType? specifiedType}) {
    final _value = firestore.Value();
    _value.booleanValue = object;
    return _value;
  }

  @override
  bool? deserialize(EntitySerializers serializers, Map fields,
      {EntityType? specifiedType}) {
    final _value = firestore.Value.fromJson(fields);
    return _value.booleanValue ?? false;
  }

  @override
  bool? deserializeValue(
      RestEntitySerializers serializers, Map<String, firestore.Value> values,
      {EntityType? specifiedType}) {
    final value = values['booleanValue'];
    return value?.booleanValue ?? false;
  }

  @override
  bool? deserializeFromJson(RestEntitySerializers serializers, bool? object,
      {EntityType? specifiedType}) {
    return object ?? false;
  }
}
