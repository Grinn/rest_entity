import 'package:built_value/built_value.dart';
import 'package:googleapis/firestore/v1.dart' as firestore;

/// A [BuiltValueToStringHelper] that produces multi-line indented output.
class IndentingValueToStringHelper implements BuiltValueToStringHelper {
  final String? name;
  final _result = StringBuffer();
  int _indentingBuiltValueToStringHelperIndent = 0;

  IndentingValueToStringHelper(firestore.Value value, {this.name}) {
    if (name != null) {
      _result.writeln(name);
    }
    _result..write(' {\n');
    _indentingBuiltValueToStringHelperIndent += 2;
  }

  @override
  void add(String field, Object? value) {
    _result
      ..write(' ' * _indentingBuiltValueToStringHelperIndent)
      ..write(field)
      ..write('=')
      ..write(value ?? 'null')
      ..write(',\n');
  }

  @override
  String toString() {
    _indentingBuiltValueToStringHelperIndent -= 2;
    _result
      ..write(' ' * _indentingBuiltValueToStringHelperIndent)
      ..write('}');
    var stringResult = _result.toString();
    return stringResult;
  }
}
