import 'package:googleapis/firestore/v1.dart' as firestore;
import 'package:rest_entity/src/core/string_utilities.dart';

extension ValueType on Map<String, firestore.Value> {
  String? valueType(String value) {
    final _value = this[value];
    if (_value?.arrayValue != null) {
      return 'arrayValue';
    }

    if (_value?.booleanValue != null) {
      return 'booleanValue';
    }

    if (_value?.bytesValue != null) {
      return 'bytesValue';
    }

    if (_value?.doubleValue != null) {
      return 'doubleValue';
    }

    if (_value?.geoPointValue != null) {
      return 'geoPointValue';
    }

    if (_value?.integerValue != null) {
      return 'integerValue';
    }

    if (_value?.mapValue != null) {
      return 'mapValue';
    }

    if (_value?.nullValue != null) {
      return 'nullValue';
    }

    if (_value?.referenceValue != null) {
      return 'referenceValue';
    }

    if (_value?.stringValue != null) {
      return 'stringValue';
    }

    if (_value?.timestampValue != null) {
      return 'timestampValue';
    }
    return null;
  }

  String toValueString() {
    final _result = StringBuffer();
    return keys
        .map((key) => _result.writeln(this[key].toValueString(name: key)))
        .join('\n');
  }
}

extension ValueString on firestore.Value? {
  String toValueString({String? name}) {
    if (this == null) {
      return '{}';
    }
    final _helper = IndentingValueToStringHelper(this!, name: name);
    _helper.add('booleanValue', this!.booleanValue?.toString());
    _helper.add('integerValue', this!.integerValue?.toString());
    _helper.add('doubleValue', this!.doubleValue?.toString());
    _helper.add('stringValue', this!.stringValue?.toString());
    _helper.add('arrayValue', this!.arrayValue?.values.toString());
    _helper.add('mapValue', this!.mapValue?.fields.toString());
    return _helper.toString();
  }
}
