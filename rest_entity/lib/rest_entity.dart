/// Support for doing something awesome.
///
/// More dartdocs go here.
library rest_entity;

export 'src/core/extension.dart';
export 'src/serializer/entity_serializer.dart';
export 'src/serializer/entity_serializers.dart';
export 'src/serializer/entity_type.dart';
