import 'package:test/test.dart';

void main() {
  group('rest entity tests', () {
    group('serialize tests', () {
      group('json tests', () {});

      group('firestore value tests', () {});
    });

    group('deserialize tests', () {
      group('json tests', () {});

      group('firestore value tests', () {});
    });
  });
}
