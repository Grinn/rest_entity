// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'person.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<Person> _$personSerializer = new _$PersonSerializer();

class _$PersonSerializer implements StructuredSerializer<Person> {
  @override
  final Iterable<Type> types = const [Person, _$Person];
  @override
  final String wireName = 'Person';

  @override
  Iterable<Object?> serialize(Serializers serializers, Person object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'age',
      serializers.serialize(object.age, specifiedType: const FullType(int)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'height',
      serializers.serialize(object.height,
          specifiedType: const FullType(double)),
      'weight',
      serializers.serialize(object.weight,
          specifiedType: const FullType(double)),
      'hobbies',
      serializers.serialize(object.hobbies,
          specifiedType:
              const FullType(BuiltList, const [const FullType(String)])),
      'traits',
      serializers.serialize(object.traits,
          specifiedType: const FullType(BuiltMap,
              const [const FullType(String), const FullType(String)])),
      'address',
      serializers.serialize(object.address,
          specifiedType: const FullType(Address)),
    ];
    Object? value;
    value = object.id;
    if (value != null) {
      result
        ..add('id')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  Person deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new PersonBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'age':
          result.age = serializers.deserialize(value,
              specifiedType: const FullType(int))! as int;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'height':
          result.height = serializers.deserialize(value,
              specifiedType: const FullType(double))! as double;
          break;
        case 'weight':
          result.weight = serializers.deserialize(value,
              specifiedType: const FullType(double))! as double;
          break;
        case 'hobbies':
          result.hobbies.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(String)]))!
              as BuiltList<Object?>);
          break;
        case 'traits':
          result.traits.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltMap,
                  const [const FullType(String), const FullType(String)]))!);
          break;
        case 'address':
          result.address.replace(serializers.deserialize(value,
              specifiedType: const FullType(Address))! as Address);
          break;
      }
    }

    return result.build();
  }
}

class _$Person extends Person {
  @override
  final String? id;
  @override
  final int age;
  @override
  final String name;
  @override
  final double height;
  @override
  final double weight;
  @override
  final BuiltList<String> hobbies;
  @override
  final BuiltMap<String, String> traits;
  @override
  final Address address;

  factory _$Person([void Function(PersonBuilder)? updates]) =>
      (new PersonBuilder()..update(updates))._build();

  _$Person._(
      {this.id,
      required this.age,
      required this.name,
      required this.height,
      required this.weight,
      required this.hobbies,
      required this.traits,
      required this.address})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(age, r'Person', 'age');
    BuiltValueNullFieldError.checkNotNull(name, r'Person', 'name');
    BuiltValueNullFieldError.checkNotNull(height, r'Person', 'height');
    BuiltValueNullFieldError.checkNotNull(weight, r'Person', 'weight');
    BuiltValueNullFieldError.checkNotNull(hobbies, r'Person', 'hobbies');
    BuiltValueNullFieldError.checkNotNull(traits, r'Person', 'traits');
    BuiltValueNullFieldError.checkNotNull(address, r'Person', 'address');
  }

  @override
  Person rebuild(void Function(PersonBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PersonBuilder toBuilder() => new PersonBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Person &&
        id == other.id &&
        age == other.age &&
        name == other.name &&
        height == other.height &&
        weight == other.weight &&
        hobbies == other.hobbies &&
        traits == other.traits &&
        address == other.address;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, age.hashCode);
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, height.hashCode);
    _$hash = $jc(_$hash, weight.hashCode);
    _$hash = $jc(_$hash, hobbies.hashCode);
    _$hash = $jc(_$hash, traits.hashCode);
    _$hash = $jc(_$hash, address.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'Person')
          ..add('id', id)
          ..add('age', age)
          ..add('name', name)
          ..add('height', height)
          ..add('weight', weight)
          ..add('hobbies', hobbies)
          ..add('traits', traits)
          ..add('address', address))
        .toString();
  }
}

class PersonBuilder implements Builder<Person, PersonBuilder> {
  _$Person? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  int? _age;
  int? get age => _$this._age;
  set age(int? age) => _$this._age = age;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  double? _height;
  double? get height => _$this._height;
  set height(double? height) => _$this._height = height;

  double? _weight;
  double? get weight => _$this._weight;
  set weight(double? weight) => _$this._weight = weight;

  ListBuilder<String>? _hobbies;
  ListBuilder<String> get hobbies =>
      _$this._hobbies ??= new ListBuilder<String>();
  set hobbies(ListBuilder<String>? hobbies) => _$this._hobbies = hobbies;

  MapBuilder<String, String>? _traits;
  MapBuilder<String, String> get traits =>
      _$this._traits ??= new MapBuilder<String, String>();
  set traits(MapBuilder<String, String>? traits) => _$this._traits = traits;

  AddressBuilder? _address;
  AddressBuilder get address => _$this._address ??= new AddressBuilder();
  set address(AddressBuilder? address) => _$this._address = address;

  PersonBuilder();

  PersonBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _age = $v.age;
      _name = $v.name;
      _height = $v.height;
      _weight = $v.weight;
      _hobbies = $v.hobbies.toBuilder();
      _traits = $v.traits.toBuilder();
      _address = $v.address.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Person other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Person;
  }

  @override
  void update(void Function(PersonBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  Person build() => _build();

  _$Person _build() {
    _$Person _$result;
    try {
      _$result = _$v ??
          new _$Person._(
              id: id,
              age: BuiltValueNullFieldError.checkNotNull(age, r'Person', 'age'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, r'Person', 'name'),
              height: BuiltValueNullFieldError.checkNotNull(
                  height, r'Person', 'height'),
              weight: BuiltValueNullFieldError.checkNotNull(
                  weight, r'Person', 'weight'),
              hobbies: hobbies.build(),
              traits: traits.build(),
              address: address.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'hobbies';
        hobbies.build();
        _$failedField = 'traits';
        traits.build();
        _$failedField = 'address';
        address.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'Person', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint

// **************************************************************************
// RestEntityGenerator
// **************************************************************************

EntitySerializer<Person> _$personEntitySerializer = _$PersonEntitySerializer();

class _$PersonEntitySerializer implements StructuredEntitySerializer<Person> {
  @override
  final Iterable<EntityType> types = const [
    EntityType(Person),
    EntityType(_$Person)
  ];
  @override
  final String wireName = 'Person';

  @override
  Map<String, Object> serialize(
    EntitySerializers serializers,
    Person object, {
    EntityType? specifiedType,
  }) {
    final _entity = <String, Object>{};
    if (object.id != null) {
      _entity['id'] = serializers.serialize(
        object.id!,
        specifiedType: const EntityType(String),
      );
    }

    _entity['age'] = serializers.serialize(
      object.age,
      specifiedType: const EntityType(int),
    );

    _entity['name'] = serializers.serialize(
      object.name,
      specifiedType: const EntityType(String),
    );

    _entity['height'] = serializers.serialize(
      object.height,
      specifiedType: const EntityType(double),
    );

    _entity['weight'] = serializers.serialize(
      object.weight,
      specifiedType: const EntityType(double),
    );

    _entity['hobbies'] = serializers.serialize(
      object.hobbies,
      specifiedType: const EntityType(
        BuiltList,
        const [const EntityType(String)],
      ),
    );

    _entity['traits'] = serializers.serialize(
      object.traits,
      specifiedType: const EntityType(
        BuiltMap,
        const [const EntityType(String), const EntityType(String)],
      ),
    );

    _entity['address'] = serializers.serialize(
      object.address,
      specifiedType: const EntityType(Address),
    );

    final _mapValue = <String, Object>{};
    final _fields = <String, Object>{'fields': _entity};
    _mapValue['mapValue'] = _fields;
    return _mapValue;
  }

  @override
  firestore.Value serializeValue(
    EntitySerializers serializers,
    Person object, {
    EntityType? specifiedType,
  }) {
    final _value = firestore.Value();
    final _mapValue = firestore.MapValue();
    _mapValue.fields = {};

    if (object.id != null) {
      _mapValue.fields!['id'] = serializers.serializeValue(
        object.id,
        specifiedType: const EntityType(String),
      );
    }

    _mapValue.fields!['age'] = serializers.serializeValue(
      object.age,
      specifiedType: const EntityType(int),
    );

    _mapValue.fields!['name'] = serializers.serializeValue(
      object.name,
      specifiedType: const EntityType(String),
    );

    _mapValue.fields!['height'] = serializers.serializeValue(
      object.height,
      specifiedType: const EntityType(double),
    );

    _mapValue.fields!['weight'] = serializers.serializeValue(
      object.weight,
      specifiedType: const EntityType(double),
    );

    _mapValue.fields!['hobbies'] = serializers.serializeValue(
      object.hobbies,
      specifiedType: const EntityType(
        BuiltList,
        const [const EntityType(String)],
      ),
    );

    _mapValue.fields!['traits'] = serializers.serializeValue(
      object.traits,
      specifiedType: const EntityType(
        BuiltMap,
        const [const EntityType(String), const EntityType(String)],
      ),
    );

    _mapValue.fields!['address'] = serializers.serializeValue(
      object.address,
      specifiedType: const EntityType(Address),
    );

    _value.mapValue = _mapValue;
    return _value;
  }

  @override
  Person deserialize(
    EntitySerializers serializers,
    Map fields, {
    EntityType? specifiedType,
  }) {
    Map _fields = fields;
    if (_fields['mapValue'] != null) {
      _fields = (_fields['mapValue'] as Map<String, Object>?)?['fields'] as Map;
    }
    return Person(
      (b) => b
        ..id = serializers.deserialize(
          _fields['id'],
          specifiedType: const EntityType(String),
        ) as String?
        ..age = serializers.deserialize(
          _fields['age'],
          specifiedType: const EntityType(int),
        ) as int
        ..name = serializers.deserialize(
          _fields['name'],
          specifiedType: const EntityType(String),
        ) as String
        ..height = serializers.deserialize(
          _fields['height'],
          specifiedType: const EntityType(double),
        ) as double
        ..weight = serializers.deserialize(
          _fields['weight'],
          specifiedType: const EntityType(double),
        ) as double
        ..hobbies = (serializers.deserialize(
          _fields['hobbies'],
          specifiedType: const EntityType(
            BuiltList,
            const [const EntityType(String)],
          ),
        ) as BuiltList)
            .toBuilder() as ListBuilder<String>
        ..traits = (serializers.deserialize(
          _fields['traits'],
          specifiedType: const EntityType(
            BuiltMap,
            const [const EntityType(String), const EntityType(String)],
          ),
        ) as BuiltMap)
            .toBuilder() as MapBuilder<String, String>
        ..address = (serializers.deserialize(
          _fields['address'],
          specifiedType: const EntityType(Address),
        ) as Address)
            .toBuilder(),
    );
  }

  @override
  Person deserializeValue(
    EntitySerializers serializers,
    Map<String, firestore.Value> values, {
    EntityType? specifiedType,
  }) {
    Map<String, firestore.Value> _values = values;
    if (values['mapValue'] != null) {
      _values = values['mapValue']!.mapValue!.fields!;
    }
    return Person(
      (b) => b
        ..id = !_values.containsKey('id')
            ? null
            : serializers.deserializeValue(
                {_values.valueType('id')!: _values['id'] ?? firestore.Value()},
                specifiedType: const EntityType(String),
              ) as String?
        ..age = !_values.containsKey('age')
            ? null
            : serializers.deserializeValue(
                {
                  _values.valueType('age')!: _values['age'] ?? firestore.Value()
                },
                specifiedType: const EntityType(int),
              ) as int
        ..name = !_values.containsKey('name')
            ? null
            : serializers.deserializeValue(
                {
                  _values.valueType('name')!:
                      _values['name'] ?? firestore.Value()
                },
                specifiedType: const EntityType(String),
              ) as String
        ..height = !_values.containsKey('height')
            ? null
            : serializers.deserializeValue(
                {
                  _values.valueType('height')!:
                      _values['height'] ?? firestore.Value()
                },
                specifiedType: const EntityType(double),
              ) as double
        ..weight = !_values.containsKey('weight')
            ? null
            : serializers.deserializeValue(
                {
                  _values.valueType('weight')!:
                      _values['weight'] ?? firestore.Value()
                },
                specifiedType: const EntityType(double),
              ) as double
        ..hobbies = !_values.containsKey('hobbies')
            ? null
            : (serializers.deserializeValue(
                {
                  _values.valueType('hobbies')!:
                      _values['hobbies'] ?? firestore.Value()
                },
                specifiedType: const EntityType(
                  BuiltList,
                  const [const EntityType(String)],
                ),
              ) as BuiltList)
                .toBuilder() as ListBuilder<String>
        ..traits = !_values.containsKey('traits')
            ? null
            : (serializers.deserializeValue(
                {
                  _values.valueType('traits')!:
                      _values['traits'] ?? firestore.Value()
                },
                specifiedType: const EntityType(
                  BuiltMap,
                  const [const EntityType(String), const EntityType(String)],
                ),
              ) as BuiltMap)
                .toBuilder() as MapBuilder<String, String>
        ..address = !_values.containsKey('address')
            ? null
            : (serializers.deserializeValue(
                {
                  _values.valueType('address')!:
                      _values['address'] ?? firestore.Value()
                },
                specifiedType: const EntityType(Address),
              ) as Address)
                .toBuilder(),
    );
  }

  Person deserializeFromJson(
    EntitySerializers serializers,
    Map? fields, {
    EntityType? specifiedType,
  }) {
    return Person.fromJson(fields ?? {});
  }
}
