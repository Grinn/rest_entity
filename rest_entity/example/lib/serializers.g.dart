// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'serializers.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializers _$serializers = (new Serializers().toBuilder()
      ..add(Address.serializer)
      ..add(Gender.serializer)
      ..add(Person.serializer)
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(
              BuiltMap, const [const FullType(String), const FullType(String)]),
          () => new MapBuilder<String, String>()))
    .build();

// ignore_for_file: deprecated_member_use_from_same_package,type=lint

// **************************************************************************
// RestEntitySerializerGenerator
// **************************************************************************

final $entitySerializers = (EntitySerializers().toBuilder()
      ..add(Person.entitySerializer)
      ..add(Address.entitySerializer)
      ..add(Gender.entitySerializer)
      ..addBuilderFactory(
        const EntityType(BuiltList, [EntityType(Person)]),
        () => ListBuilder<Person>(),
      )
      ..addBuilderFactory(
        const EntityType(BuiltList, [EntityType(Address)]),
        () => ListBuilder<Address>(),
      )
      ..addBuilderFactory(
        const EntityType(BuiltList, [EntityType(Gender)]),
        () => ListBuilder<Gender>(),
      ))
    .build();
