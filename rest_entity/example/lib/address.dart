import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:googleapis/firestore/v1.dart' as firestore;
import 'package:rest_entity/rest_entity.dart';
import 'package:rest_entity_test/serializers.dart';

part 'address.g.dart';

abstract class Address implements Built<Address, AddressBuilder> {
  static Serializer<Address> get serializer => _$addressSerializer;
  static EntitySerializer<Address> get entitySerializer =>
      _$addressEntitySerializer;

  String? get id;
  String get street;
  int get houseNumber;
  String get zipCode;
  String get city;

  factory Address([Function(AddressBuilder b) updates]) = _$Address;

  Address._();

  factory Address.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json)!;
  }

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(serializer, this) as Map<String, dynamic>;
  }

  factory Address.fromEntity(Map json) {
    return entitySerializers.deserializeWith(entitySerializer, json);
  }

  Map<String, dynamic> toEntity() {
    return entitySerializers.serializeWith(entitySerializer, this)
        as Map<String, dynamic>;
  }

  factory Address.fromValue(Map<String, firestore.Value> value) {
    return entitySerializers.deserializeValueWith(entitySerializer, value);
  }

  firestore.Value toValue() {
    return entitySerializers.serializeValueWith(entitySerializer, this);
  }
}
