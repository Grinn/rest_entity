import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:googleapis/firestore/v1.dart' as firestore;
import 'package:rest_entity/rest_entity.dart';
import 'package:rest_entity_test/address.dart';

import 'package:rest_entity_test/serializers.dart';

part 'person.g.dart';

abstract class Person implements Built<Person, PersonBuilder> {
  static Serializer<Person> get serializer => _$personSerializer;
  static EntitySerializer<Person> get entitySerializer =>
      _$personEntitySerializer;

  String? get id;
  int get age;
  String get name;
  double get height;
  double get weight;
  BuiltList<String> get hobbies;
  BuiltMap<String, String> get traits;
  Address get address;

  factory Person([Function(PersonBuilder b) updates]) = _$Person;

  Person._();

  factory Person.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json)!;
  }

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(serializer, this) as Map<String, dynamic>;
  }

  factory Person.fromEntity(Map json) {
    return entitySerializers.deserializeWith(entitySerializer, json);
  }

  Map<String, dynamic> toEntity() {
    return entitySerializers.serializeWith(entitySerializer, this)
        as Map<String, dynamic>;
  }

  factory Person.fromValue(Map<String, firestore.Value> value) {
    return entitySerializers.deserializeValueWith(entitySerializer, value);
  }

  firestore.Value toValue() {
    return entitySerializers.serializeValueWith(entitySerializer, this);
  }
}
