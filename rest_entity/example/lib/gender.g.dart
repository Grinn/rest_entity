// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gender.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const Gender _$male = const Gender._('male');
const Gender _$female = const Gender._('female');
const Gender _$other = const Gender._('other');

Gender _$valueOf(String name) {
  switch (name) {
    case 'male':
      return _$male;
    case 'female':
      return _$female;
    case 'other':
      return _$other;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<Gender> _$values = new BuiltSet<Gender>(const <Gender>[
  _$male,
  _$female,
  _$other,
]);

Serializer<Gender> _$genderSerializer = new _$GenderSerializer();

class _$GenderSerializer implements PrimitiveSerializer<Gender> {
  @override
  final Iterable<Type> types = const <Type>[Gender];
  @override
  final String wireName = 'Gender';

  @override
  Object serialize(Serializers serializers, Gender object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  Gender deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      Gender.valueOf(serialized as String);
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint

// **************************************************************************
// RestEntityGenerator
// **************************************************************************

EntitySerializer<Gender> _$genderEntitySerializer = _$GenderEntitySerializer();

class _$GenderEntitySerializer implements StructuredEntitySerializer<Gender> {
  @override
  final Iterable<EntityType> types = const [EntityType(Gender)];
  @override
  final String wireName = 'Gender';

  @override
  Map<String, Object> serialize(
    EntitySerializers serializers,
    Gender object, {
    EntityType? specifiedType,
  }) {
    return <String, Object>{'stringValue': object.name};
  }

  @override
  firestore.Value serializeValue(
    EntitySerializers serializers,
    Gender object, {
    EntityType? specifiedType,
  }) {
    final _value = firestore.Value();
    _value.stringValue = object.name;
    return _value;
  }

  @override
  Gender deserialize(
    EntitySerializers serializers,
    Map fields, {
    EntityType? specifiedType,
  }) {
    return Gender.valueOf(fields['stringValue'] as String);
  }

  @override
  Gender deserializeValue(
    EntitySerializers serializers,
    Map<String, firestore.Value> values, {
    EntityType? specifiedType,
  }) {
    Map<String, firestore.Value> _values = values;
    if (values['mapValue'] != null) {
      _values = values['mapValue']!.mapValue!.fields!;
    }
    return Gender.valueOf(_values['stringValue']!.stringValue!);
  }

  Gender deserializeFromJson(
    EntitySerializers serializers,
    Map? fields, {
    EntityType? specifiedType,
  }) {
    return Gender.valueOf(fields!.values.first as String);
  }
}
