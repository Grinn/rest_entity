// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'address.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<Address> _$addressSerializer = new _$AddressSerializer();

class _$AddressSerializer implements StructuredSerializer<Address> {
  @override
  final Iterable<Type> types = const [Address, _$Address];
  @override
  final String wireName = 'Address';

  @override
  Iterable<Object?> serialize(Serializers serializers, Address object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'street',
      serializers.serialize(object.street,
          specifiedType: const FullType(String)),
      'houseNumber',
      serializers.serialize(object.houseNumber,
          specifiedType: const FullType(int)),
      'zipCode',
      serializers.serialize(object.zipCode,
          specifiedType: const FullType(String)),
      'city',
      serializers.serialize(object.city, specifiedType: const FullType(String)),
    ];
    Object? value;
    value = object.id;
    if (value != null) {
      result
        ..add('id')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  Address deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new AddressBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'street':
          result.street = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'houseNumber':
          result.houseNumber = serializers.deserialize(value,
              specifiedType: const FullType(int))! as int;
          break;
        case 'zipCode':
          result.zipCode = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'city':
          result.city = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
      }
    }

    return result.build();
  }
}

class _$Address extends Address {
  @override
  final String? id;
  @override
  final String street;
  @override
  final int houseNumber;
  @override
  final String zipCode;
  @override
  final String city;

  factory _$Address([void Function(AddressBuilder)? updates]) =>
      (new AddressBuilder()..update(updates))._build();

  _$Address._(
      {this.id,
      required this.street,
      required this.houseNumber,
      required this.zipCode,
      required this.city})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(street, r'Address', 'street');
    BuiltValueNullFieldError.checkNotNull(
        houseNumber, r'Address', 'houseNumber');
    BuiltValueNullFieldError.checkNotNull(zipCode, r'Address', 'zipCode');
    BuiltValueNullFieldError.checkNotNull(city, r'Address', 'city');
  }

  @override
  Address rebuild(void Function(AddressBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AddressBuilder toBuilder() => new AddressBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Address &&
        id == other.id &&
        street == other.street &&
        houseNumber == other.houseNumber &&
        zipCode == other.zipCode &&
        city == other.city;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, street.hashCode);
    _$hash = $jc(_$hash, houseNumber.hashCode);
    _$hash = $jc(_$hash, zipCode.hashCode);
    _$hash = $jc(_$hash, city.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'Address')
          ..add('id', id)
          ..add('street', street)
          ..add('houseNumber', houseNumber)
          ..add('zipCode', zipCode)
          ..add('city', city))
        .toString();
  }
}

class AddressBuilder implements Builder<Address, AddressBuilder> {
  _$Address? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _street;
  String? get street => _$this._street;
  set street(String? street) => _$this._street = street;

  int? _houseNumber;
  int? get houseNumber => _$this._houseNumber;
  set houseNumber(int? houseNumber) => _$this._houseNumber = houseNumber;

  String? _zipCode;
  String? get zipCode => _$this._zipCode;
  set zipCode(String? zipCode) => _$this._zipCode = zipCode;

  String? _city;
  String? get city => _$this._city;
  set city(String? city) => _$this._city = city;

  AddressBuilder();

  AddressBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _street = $v.street;
      _houseNumber = $v.houseNumber;
      _zipCode = $v.zipCode;
      _city = $v.city;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Address other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Address;
  }

  @override
  void update(void Function(AddressBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  Address build() => _build();

  _$Address _build() {
    final _$result = _$v ??
        new _$Address._(
            id: id,
            street: BuiltValueNullFieldError.checkNotNull(
                street, r'Address', 'street'),
            houseNumber: BuiltValueNullFieldError.checkNotNull(
                houseNumber, r'Address', 'houseNumber'),
            zipCode: BuiltValueNullFieldError.checkNotNull(
                zipCode, r'Address', 'zipCode'),
            city: BuiltValueNullFieldError.checkNotNull(
                city, r'Address', 'city'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint

// **************************************************************************
// RestEntityGenerator
// **************************************************************************

EntitySerializer<Address> _$addressEntitySerializer =
    _$AddressEntitySerializer();

class _$AddressEntitySerializer implements StructuredEntitySerializer<Address> {
  @override
  final Iterable<EntityType> types = const [
    EntityType(Address),
    EntityType(_$Address)
  ];
  @override
  final String wireName = 'Address';

  @override
  Map<String, Object> serialize(
    EntitySerializers serializers,
    Address object, {
    EntityType? specifiedType,
  }) {
    final _entity = <String, Object>{};
    if (object.id != null) {
      _entity['id'] = serializers.serialize(
        object.id!,
        specifiedType: const EntityType(String),
      );
    }

    _entity['street'] = serializers.serialize(
      object.street,
      specifiedType: const EntityType(String),
    );

    _entity['houseNumber'] = serializers.serialize(
      object.houseNumber,
      specifiedType: const EntityType(int),
    );

    _entity['zipCode'] = serializers.serialize(
      object.zipCode,
      specifiedType: const EntityType(String),
    );

    _entity['city'] = serializers.serialize(
      object.city,
      specifiedType: const EntityType(String),
    );

    final _mapValue = <String, Object>{};
    final _fields = <String, Object>{'fields': _entity};
    _mapValue['mapValue'] = _fields;
    return _mapValue;
  }

  @override
  firestore.Value serializeValue(
    EntitySerializers serializers,
    Address object, {
    EntityType? specifiedType,
  }) {
    final _value = firestore.Value();
    final _mapValue = firestore.MapValue();
    _mapValue.fields = {};

    if (object.id != null) {
      _mapValue.fields!['id'] = serializers.serializeValue(
        object.id,
        specifiedType: const EntityType(String),
      );
    }

    _mapValue.fields!['street'] = serializers.serializeValue(
      object.street,
      specifiedType: const EntityType(String),
    );

    _mapValue.fields!['houseNumber'] = serializers.serializeValue(
      object.houseNumber,
      specifiedType: const EntityType(int),
    );

    _mapValue.fields!['zipCode'] = serializers.serializeValue(
      object.zipCode,
      specifiedType: const EntityType(String),
    );

    _mapValue.fields!['city'] = serializers.serializeValue(
      object.city,
      specifiedType: const EntityType(String),
    );

    _value.mapValue = _mapValue;
    return _value;
  }

  @override
  Address deserialize(
    EntitySerializers serializers,
    Map fields, {
    EntityType? specifiedType,
  }) {
    Map _fields = fields;
    if (_fields['mapValue'] != null) {
      _fields = (_fields['mapValue'] as Map<String, Object>?)?['fields'] as Map;
    }
    return Address(
      (b) => b
        ..id = serializers.deserialize(
          _fields['id'],
          specifiedType: const EntityType(String),
        ) as String?
        ..street = serializers.deserialize(
          _fields['street'],
          specifiedType: const EntityType(String),
        ) as String
        ..houseNumber = serializers.deserialize(
          _fields['houseNumber'],
          specifiedType: const EntityType(int),
        ) as int
        ..zipCode = serializers.deserialize(
          _fields['zipCode'],
          specifiedType: const EntityType(String),
        ) as String
        ..city = serializers.deserialize(
          _fields['city'],
          specifiedType: const EntityType(String),
        ) as String,
    );
  }

  @override
  Address deserializeValue(
    EntitySerializers serializers,
    Map<String, firestore.Value> values, {
    EntityType? specifiedType,
  }) {
    Map<String, firestore.Value> _values = values;
    if (values['mapValue'] != null) {
      _values = values['mapValue']!.mapValue!.fields!;
    }
    return Address(
      (b) => b
        ..id = !_values.containsKey('id')
            ? null
            : serializers.deserializeValue(
                {_values.valueType('id')!: _values['id'] ?? firestore.Value()},
                specifiedType: const EntityType(String),
              ) as String?
        ..street = !_values.containsKey('street')
            ? null
            : serializers.deserializeValue(
                {
                  _values.valueType('street')!:
                      _values['street'] ?? firestore.Value()
                },
                specifiedType: const EntityType(String),
              ) as String
        ..houseNumber = !_values.containsKey('houseNumber')
            ? null
            : serializers.deserializeValue(
                {
                  _values.valueType('houseNumber')!:
                      _values['houseNumber'] ?? firestore.Value()
                },
                specifiedType: const EntityType(int),
              ) as int
        ..zipCode = !_values.containsKey('zipCode')
            ? null
            : serializers.deserializeValue(
                {
                  _values.valueType('zipCode')!:
                      _values['zipCode'] ?? firestore.Value()
                },
                specifiedType: const EntityType(String),
              ) as String
        ..city = !_values.containsKey('city')
            ? null
            : serializers.deserializeValue(
                {
                  _values.valueType('city')!:
                      _values['city'] ?? firestore.Value()
                },
                specifiedType: const EntityType(String),
              ) as String,
    );
  }

  Address deserializeFromJson(
    EntitySerializers serializers,
    Map? fields, {
    EntityType? specifiedType,
  }) {
    return Address.fromJson(fields ?? {});
  }
}
