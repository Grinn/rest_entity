import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:googleapis/firestore/v1.dart' as firestore;
import 'package:rest_entity/rest_entity.dart';

part 'gender.g.dart';

class Gender extends EnumClass {
  static Serializer<Gender> get serializer => _$genderSerializer;
  static EntitySerializer<Gender> get entitySerializer =>
      _$genderEntitySerializer;

  static const Gender male = _$male;
  static const Gender female = _$female;
  static const Gender other = _$other;

  const Gender._(String name) : super(name);

  static BuiltSet<Gender> get values => _$values;

  static Gender valueOf(String name) => _$valueOf(name);
}
