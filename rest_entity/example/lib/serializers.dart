import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:rest_entity/rest_entity.dart';
import 'package:rest_entity_test/gender.dart';

import 'address.dart';
import 'person.dart';

part 'serializers.g.dart';

@SerializersFor([
  Person,
  Address,
  Gender,
])
final Serializers serializers =
    (_$serializers.toBuilder()..addPlugin(StandardJsonPlugin())).build();

final EntitySerializers entitySerializers =
    ($entitySerializers.toBuilder()).build();
