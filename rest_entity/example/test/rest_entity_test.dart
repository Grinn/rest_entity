import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:googleapis/firestore/v1.dart';
import 'package:rest_entity_test/address.dart';
import 'package:rest_entity_test/person.dart';
import 'package:test/test.dart';

void main() {
  group('rest entity tests', () {
    final _address = Address((b) => b
      ..street = 'street'
      ..houseNumber = 10
      ..zipCode = '1234AB'
      ..city = 'City');
    final _person = Person((b) => b
      ..name = 'First LastName'
      ..age = 32
      ..height = 172.90
      ..weight = 75
      ..hobbies = ListBuilder<String>(['lifting weight', 'running', 'cycling'])
      ..traits = MapBuilder<String, String>({'physique': 'fit'})
      ..address = _address.toBuilder());

    const _personAsText =
        '''{"mapValue":{"fields":{"age":{"integerValue":"32"},"name":{"stringValue":"First LastName"},"height":{"doubleValue":172.9},"weight":{"doubleValue":75.0},"hobbies":{"arrayValue":{"values":[{"stringValue":"lifting weight"},{"stringValue":"running"},{"stringValue":"cycling"}]}},"traits":{"mapValue":{"fields":{"physique":{"stringValue":"fit"}}}},"address":{"mapValue":{"fields":{"street":{"stringValue":"street"},"houseNumber":{"integerValue":"10"},"zipCode":{"stringValue":"1234AB"},"city":{"stringValue":"City"}}}}}}}''';
    final _json = jsonDecode(_personAsText) as Map<String, dynamic>;
    group('serialize tests', () {
      group('json tests', () {});

      group('firestore value tests', () {
        final _value = _person.toValue();
        // print(jsonEncode(_value.toJson()));
      });
    });

    group('deserialize tests', () {
      group('json tests', () {});

      group('firestore value tests', () {
        final _mapValue = _json['mapValue'] as Map<String, dynamic>;
        final _document = Document.fromJson(_mapValue);
        final person = Person.fromValue(_document.fields!);
        print(jsonEncode(person.toJson()));
      });
    });
  });
}
