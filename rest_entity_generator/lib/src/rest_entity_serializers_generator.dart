import 'dart:async';

import 'package:build/build.dart';
import 'package:rest_entity_generator/src/supported_classes_finder.dart';
import 'package:source_gen/source_gen.dart';

class RestEntitySerializerGenerator extends Generator {
  @override
  FutureOr<String> generate(LibraryReader library, BuildStep buildStep) {
    final _supportedClasses = supportedClasses(library);
    if (_supportedClasses.isNotEmpty) {
      return '''final \$entitySerializers = (EntitySerializers().toBuilder()
          ${_writeSerializers(_supportedClasses)}
          ${_writeBuilderFactories(_supportedClasses)}
        .build();''';
    }
    return '';
  }

  String _writeSerializers(List<String> classes) {
    final _stringBuffer = StringBuffer();
    classes.forEach((element) {
      _stringBuffer.writeln('..add($element.entitySerializer)');
    });
    return _stringBuffer.toString();
  }

  String _writeBuilderFactories(List<String> classes) {
    final _stringBuffer = StringBuffer();
    classes.forEach((element) {
      _stringBuffer.writeln('''
      ..addBuilderFactory(
          const EntityType(BuiltList, [EntityType($element)]),
          () => ListBuilder<$element>(),)''');
    });
    _stringBuffer.write(')');
    return _stringBuffer.toString();
  }
}
