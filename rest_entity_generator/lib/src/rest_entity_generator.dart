import 'dart:async';

import 'package:analyzer/dart/constant/value.dart';
import 'package:analyzer/dart/element/element.dart';
import 'package:build/build.dart';
import 'package:source_gen/source_gen.dart';

class RestEntityGenerator extends Generator {
  @override
  FutureOr<String> generate(LibraryReader library, BuildStep buildStep) {
    final restEntity = library.classes.any((_class) {
      return _class.allSupertypes.any((_superType) {
        return _superType.getDisplayString(withNullability: false).startsWith('Built<${_class.displayName},') ||
            _superType.getDisplayString(withNullability: false) == 'EnumClass';
      });
    });
    if (restEntity) {
      final _class =
          library.classes.firstWhere((element) => element is ClassElement);
      late String result;
      if (_class.isEnumClass) {
        result = _printEnumEntitySerializer(_class, library);
      } else {
        result = _printEntitySerializer(_class, library);
      }
      return result;
    } else {
      return '';
    }
  }

  String _printEnumEntitySerializer(
      ClassElement _class, LibraryReader library) {
    return '''EntitySerializer<${_class.name}> _\$${_class.name.toLowerCaseFirstLetter()}EntitySerializer = _\$${_class.name}EntitySerializer();
    class _\$${_class.name}EntitySerializer implements StructuredEntitySerializer<${_class.name}> {
      @override
      final Iterable<EntityType> types = const [EntityType(${_class.name})];
      @override
      final String wireName = '${_class.name}';

      @override
      Map<String, Object> serialize(EntitySerializers serializers, ${_class.name} object,
          {EntityType? specifiedType,}) {
        return <String, Object>{\'stringValue\' : object.name};
      }
      
      @override
      firestore.Value serializeValue(EntitySerializers serializers, ${_class.name} object,
          {EntityType? specifiedType,}) {
        final _value = firestore.Value();
        _value.stringValue = object.name;
        return _value;
      }

      @override
      ${_class.name} deserialize(EntitySerializers serializers, Map fields, {EntityType? specifiedType,}) {
        return ${_class.name}.valueOf(fields['stringValue'] as String);
      }
      
      @override
      ${_class.name} deserializeValue(EntitySerializers serializers, Map<String, firestore.Value> values, {EntityType? specifiedType,}) {
        Map<String, firestore.Value> _values = values;
        if (values['mapValue'] != null) {
          _values = values['mapValue']!.mapValue!.fields!;
        }
        return ${_class.name}.valueOf(_values['stringValue']!.stringValue!);
      }
      
      ${_class.name} deserializeFromJson(EntitySerializers serializers, Map? fields, {EntityType? specifiedType,}) {
        return ${_class.name}.valueOf(fields!.values.first as String);
      }
    }''';
  }

  String _printEntitySerializer(ClassElement _class, LibraryReader library) {
    return '''EntitySerializer<${_class.name}> _\$${_class.name.toLowerCaseFirstLetter()}EntitySerializer = _\$${_class.name}EntitySerializer();
    class _\$${_class.name}EntitySerializer implements StructuredEntitySerializer<${_class.name}> {
      @override
      final Iterable<EntityType> types = const [EntityType(${_class.name}), EntityType(_\$${_class.name})];
      @override
      final String wireName = '${_class.name}';

      @override
      Map<String, Object> serialize(EntitySerializers serializers, ${_class.name} object,
          {EntityType? specifiedType,}) {
        final _entity = <String, Object>{};
        ${_serializeElements(library)}
        
        final _mapValue = <String, Object>{};
        final _fields = <String, Object>{
          'fields' : _entity
        };
        _mapValue['mapValue'] = _fields;
        return _mapValue;
      }

      @override
      firestore.Value serializeValue(EntitySerializers serializers, ${_class.name} object,
          {EntityType? specifiedType,}) {
        final _value = firestore.Value();
        final _mapValue = firestore.MapValue();
        _mapValue.fields = {};
        
        ${_serializeValueElements(library)}
        
        _value.mapValue = _mapValue;
        return _value;
      }      

      @override
      ${_class.name} deserialize(EntitySerializers serializers, Map fields, {EntityType? specifiedType,}) {
        Map _fields = fields;
        if (_fields['mapValue'] != null) {
          _fields = (_fields['mapValue'] as Map<String, Object>?)?['fields'] as Map;
        }
        return ${_class.name}((b) => b
          ${_deserializeElements(library)},
        );
      }
      
      @override
      ${_class.name} deserializeValue(EntitySerializers serializers, Map<String, firestore.Value> values, {EntityType? specifiedType,}) {
        Map<String, firestore.Value> _values = values;
        if (values['mapValue'] != null) {
          _values = values['mapValue']!.mapValue!.fields!;
        }
        return ${_class.name}((b) => b
          ${_deserializeValueElements(library)},
        );
      }
      
      ${_class.name} deserializeFromJson(EntitySerializers serializers, Map? fields, {EntityType? specifiedType,}) {
        return ${_class.name}.fromJson(fields ?? {});
      }
    }''';
  }

  String _deserializeElements(LibraryReader library) {
    final _stringBuffer = StringBuffer();
    library.allElements
        .whereType<ClassElement>()
        .where((element) => element.isAbstract)
        .forEach((element) {
      if (element is ClassElement) {
        final _fields = _validFields(element);
        _fields.forEach((field) {
          if (!field.isStatic && !field.isMemoized) {
            if (field.isBuiltList) {
              _stringBuffer.writeln(
                  '''..${field.name} =  (serializers.deserialize(_fields[\'${field.name}\'], specifiedType: const EntityType(BuiltList, const [const EntityType(${field.builtListType})],),) as BuiltList).toBuilder()  as ListBuilder<${field.builtListType}>''');
            } else if (field.isBuiltMap) {
              _stringBuffer.writeln(
                  '''..${field.name} =  (serializers.deserialize(_fields[\'${field.name}\'], specifiedType: const EntityType(BuiltMap, ${_deserializeBuiltMapKey(field)}, ${_deserializeBuiltMapValue(field)}],),) as BuiltMap).toBuilder() as MapBuilder<${_deserializeBuiltMapKeyType(field)}, ${_deserializeBuiltMapValueType(field)}>''');
            } else if (!field.isBuiltValue) {
              _stringBuffer.writeln(
                  '''..${field.name} =  serializers.deserialize(_fields[\'${field.name}\'], specifiedType: const EntityType(${field.type.getDisplayString(withNullability: false)}),) as ${field.type.getDisplayString(withNullability: field.isNullable)}''');
            } else {
              _stringBuffer.writeln(
                  '''..${field.name} =  (serializers.deserialize(_fields[\'${field.name}\'], specifiedType: const EntityType(${field.type.getDisplayString(withNullability: false)}),) as ${field.type.getDisplayString(withNullability: field.isNullable)})${_addNullability(field)}.toBuilder()''');
            }
          }
        });
      }
    });
    return _stringBuffer.toString();
  }

  Iterable<FieldElement> _validFields(ClassElement element) {
    final _fields = element.fields.where((field) =>
        ['types', 'wireName', 'hashCode'].contains(field.name) == false &&
        field.name.startsWith('_') == false &&
        field.type
                .getDisplayString(withNullability: false)
                .contains('Serializer') ==
            false);
    // print('_fields are $_fields');
    return _fields.toSet().toList();
  }

  String _deserializeValueElements(LibraryReader library) {
    final _stringBuffer = StringBuffer();
    library.allElements
        .whereType<ClassElement>()
        .where((element) => element.isAbstract)
        .forEach((element) {
      if (element is ClassElement) {
        // print('element is ${element.toString()}');
        final _fields = _validFields(element);
        // print('fields are ${_fields}');
        _fields.forEach((field) {
          if (!field.isStatic && !field.isMemoized) {
            if (field.isBuiltList) {
              _stringBuffer.writeln(
                  '''..${field.name} = !_values.containsKey(\'${field.name}\') ? null : (serializers.deserializeValue({_values.valueType(\'${field.name}\')! : _values[\'${field.name}\'] ?? firestore.Value()}, specifiedType: const EntityType(BuiltList, const [const EntityType(${field.builtListType})],),) as BuiltList).toBuilder()  as ListBuilder<${field.builtListType}>''');
            } else if (field.isBuiltMap) {
              _stringBuffer.writeln(
                  '''..${field.name} = !_values.containsKey(\'${field.name}\') ? null :  (serializers.deserializeValue({_values.valueType(\'${field.name}\')! : _values[\'${field.name}\'] ?? firestore.Value()}, specifiedType: const EntityType(BuiltMap, ${_deserializeBuiltMapKey(field)}, ${_deserializeBuiltMapValue(field)}],),) as BuiltMap).toBuilder() as MapBuilder<${_deserializeBuiltMapKeyType(field)}, ${_deserializeBuiltMapValueType(field)}>''');
            } else if (!field.isBuiltValue) {
              _stringBuffer.writeln(
                  '''..${field.name} = !_values.containsKey(\'${field.name}\') ? null :  serializers.deserializeValue({_values.valueType(\'${field.name}\')! : _values[\'${field.name}\'] ?? firestore.Value()}, specifiedType: const EntityType(${field.type.getDisplayString(withNullability: false)}),) as ${field.type.getDisplayString(withNullability: field.isNullable)}''');
            } else {
              _stringBuffer.writeln(
                  '''..${field.name} = !_values.containsKey(\'${field.name}\') ? null :  (serializers.deserializeValue({_values.valueType(\'${field.name}\')! : _values[\'${field.name}\'] ?? firestore.Value()}, specifiedType: const EntityType(${field.type.getDisplayString(withNullability: false)}),) as ${field.type.getDisplayString(withNullability: field.isNullable)})${_addNullability(field)}.toBuilder()''');
            }
          }
        });
      }
    });
    return _stringBuffer.toString();
  }

  String _addNullability(FieldElement field) {
    return field.isNullable ? '?' : '';
  }

  String _deserializeBuiltMapKey(FieldElement field) {
    if (field.builtMapKeyType.contains('BuiltList')) {
      return 'const EntityType(BuiltList, const [const EntityType(${field.builtMapKeyType.extractBuiltListType()})])';
    } else {
      return 'const [const EntityType(${field.builtMapKeyType})';
    }
  }

  String _deserializeBuiltMapKeyType(FieldElement field) {
    if (field.builtMapKeyType.contains('BuiltList')) {
      return 'BuiltList<${field.builtMapValueType.extractBuiltListType()}>';
    } else {
      return field.builtMapKeyType;
    }
  }

  String _deserializeBuiltMapValue(FieldElement field) {
    if (field.builtMapValueType.contains('BuiltList')) {
      return 'const EntityType(BuiltList, const [const EntityType(${field.builtMapValueType.extractBuiltListType()})])';
    } else {
      return 'const EntityType(${field.builtMapValueType})';
    }
  }

  String _deserializeBuiltMapValueType(FieldElement field) {
    if (field.builtMapValueType.contains('BuiltList')) {
      return 'BuiltList<${field.builtMapValueType.extractBuiltListType()}>';
    } else {
      return field.builtMapValueType;
    }
  }

  String _serializeElements(LibraryReader library) {
    final _stringBuffer = StringBuffer();
    library.allElements
        .whereType<ClassElement>()
        .where((element) => element.isAbstract)
        .forEach((element) {
      if (element is ClassElement) {
        final _fields = _validFields(element);
        _fields.forEach((field) {
          if (!field.isStatic && !field.isMemoized) {
            if (field.isBuiltList) {
              _stringBuffer.writeln('''
              ${field.isNullable ? 'if (object.${field.name} != null) {' : ''} 
                _entity[\'${field.name}\'] = serializers.serialize(object.${field.name}${field.isNullable ? '!' : ''},specifiedType: const EntityType(BuiltList, const [const EntityType(${field.builtListType})],),);
              ${field.isNullable ? '}' : ''}''');
            } else if (field.isBuiltMap) {
              _stringBuffer.writeln('''
              ${field.isNullable ? 'if (object.${field.name} != null) {' : ''} 
                _entity[\'${field.name}\'] = serializers.serialize(object.${field.name}${field.isNullable ? '!' : ''}, specifiedType: const EntityType(BuiltMap, ${_deserializeBuiltMapKey(field)}, ${_deserializeBuiltMapValue(field)}],),);
              ${field.isNullable ? '}' : ''}''');
            } else if (!field.isBuiltValue) {
              _stringBuffer.writeln('''
            ${field.isNullable ? 'if (object.${field.name} != null) {' : ''}
              _entity[\'${field.name}\'] = serializers.serialize(object.${field.name}${field.isNullable ? '!' : ''}, specifiedType: const EntityType(${field.type.getDisplayString(withNullability: false)}),);
            ${field.isNullable ? '}' : ''}
            ''');
            } else {
              _stringBuffer.writeln('''
            ${field.isNullable ? 'if (object.${field.name} != null) {' : ''}
              _entity[\'${field.name}\'] = serializers.serialize(object.${field.name}${field.isNullable ? '!' : ''}, specifiedType: const EntityType(${field.type.getDisplayString(withNullability: false)}),);
            ${field.isNullable ? '}' : ''}
            ''');
            }
          }
        });
      }
    });
    return _stringBuffer.toString();
  }

  String _serializeValueElements(LibraryReader library) {
    final _stringBuffer = StringBuffer();
    library.allElements
        .whereType<ClassElement>()
        .where((element) => element.isAbstract)
        .forEach((element) {
      if (element is ClassElement) {
        final _fields = _validFields(element);
        _fields.forEach((field) {
          if (!field.isStatic && !field.isMemoized) {
            if (field.isBuiltList) {
              _stringBuffer.writeln('''
              ${field.isNullable ? 'if (object.${field.name} != null) {' : ''} 
                _mapValue.fields![\'${field.name}\'] = serializers.serializeValue(object.${field.name},specifiedType: const EntityType(BuiltList, const [const EntityType(${field.builtListType})],),);
              ${field.isNullable ? '}' : ''}''');
            } else if (field.isBuiltMap) {
              _stringBuffer.writeln('''
              ${field.isNullable ? 'if (object.${field.name} != null) {' : ''} 
                _mapValue.fields![\'${field.name}\'] = serializers.serializeValue(object.${field.name}, specifiedType: const EntityType(BuiltMap, ${_deserializeBuiltMapKey(field)}, ${_deserializeBuiltMapValue(field)}],),);
              ${field.isNullable ? '}' : ''}''');
            } else if (!field.isBuiltValue) {
              _stringBuffer.writeln('''
            ${field.isNullable ? 'if (object.${field.name} != null) {' : ''}
              _mapValue.fields![\'${field.name}\'] = serializers.serializeValue(object.${field.name}, specifiedType: const EntityType(${field.type.getDisplayString(withNullability: false)}),);
            ${field.isNullable ? '}' : ''}
            ''');
            } else {
              _stringBuffer.writeln('''
            ${field.isNullable ? 'if (object.${field.name} != null) {' : ''}
              _mapValue.fields![\'${field.name}\'] = serializers.serializeValue(object.${field.name}, specifiedType: const EntityType(${field.type.getDisplayString(withNullability: false)}),);
            ${field.isNullable ? '}' : ''}
            ''');
            }
          }
        });
      }
    });
    return _stringBuffer.toString();
  }
}

extension UpperCaseFirstLetterString on String {
  String toUpperCaseFirstLetter() {
    return '${substring(0, 1).toUpperCase()}${substring(1, length)}';
  }

  String toLowerCaseFirstLetter() {
    return '${substring(0, 1).toLowerCase()}${substring(1, length)}';
  }

  String extractBuiltListType() {
    final regex = RegExp(r'<[a-zA-Z0-9]*>$');
    final _match = regex.firstMatch(this)!.group(0)!;
    return _match.substring(1, _match.length - 1);
  }
}

extension EnumClassElement on ClassElement {
  bool get isEnumClass {
    return supertype!.getDisplayString(withNullability: false) == 'EnumClass';
  }

  bool get isBuiltValue {
    return allSupertypes.any((interfaceType) {
      return interfaceType.element.name == 'Built';
    });
  }
}

extension BuiltFieldElement on FieldElement {
  bool get isBuiltList {
    final regex = RegExp(r'^BuiltList<[a-zA-Z0-9]*>$');
    return regex.hasMatch(type.getDisplayString(withNullability: false));
  }

  String get builtListType {
    final regex = RegExp(r'<[a-zA-Z0-9]*>$');
    final _match = regex
        .firstMatch(type.getDisplayString(withNullability: false))!
        .group(0)!;

    return _match.substring(1, _match.length - 1);
  }

  bool get isBuiltMap {
    final regex = RegExp(r'^BuiltMap<.*, .*>$');
    return regex.hasMatch(type.getDisplayString(withNullability: false));
  }

  String get builtMapKeyType {
    final regex = RegExp(r'<.*,');
    final _match = regex
        .firstMatch(type.getDisplayString(withNullability: false))!
        .group(0)!;

    return _match.substring(1, _match.length - 1);
  }

  String get builtMapValueType {
    final regex = RegExp(r',.*>$');
    final _match = regex
        .firstMatch(type.getDisplayString(withNullability: false))!
        .group(0)!;
    return _match.substring(1, _match.length - 1);
  }

  bool get isDateTime {
    return type.getDisplayString(withNullability: false) == 'DateTime';
  }

  bool get isBuiltValue {
    if (type.element is! ClassElement) return false;
    return (type.element as ClassElement)
        .allSupertypes
        .any((interfaceType) => interfaceType.element.name == 'Built');
  }

  bool get isNullable {
    return getter != null &&
            getter!.metadata.any(
                (element) => metadataToStringValue(element) == 'nullable') ||
        getDisplayString(withNullability: true).contains('*') ||
        getDisplayString(withNullability: true).contains('?');
  }

  bool get isMemoized {
    return getter != null &&
        getter!.metadata
            .any((element) => metadataToStringValue(element) == 'memoized');
  }

  bool get isDartCoreMember {
    return type.isDartCoreBool ||
        type.isDartCoreDouble ||
        type.isDartCoreInt ||
        type.isDartCoreNull ||
        type.isDartCoreString ||
        isDateTime;
  }
}

DartObject getConstantValueFromAnnotation(ElementAnnotation annotation) {
  final value = annotation.computeConstantValue();
  if (value == null) {
    throw InvalidGenerationSourceError(
        'Can’t process annotation “${annotation.toSource()}” in '
        '“${annotation.librarySource!.uri}”. Please check for a missing import.');
  }
  return value;
}

/// Gets the `String` value of an annotation. Throws a descriptive
/// [InvalidGenerationSourceError] if the annotation can't be resolved.
String? metadataToStringValue(ElementAnnotation annotation) {
  final value = getConstantValueFromAnnotation(annotation);
  return value.toStringValue();
}
