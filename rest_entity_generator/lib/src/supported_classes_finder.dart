import 'package:analyzer/dart/element/element.dart';
import 'package:analyzer/dart/element/type.dart';
import 'package:built_value/serializer.dart';
import 'package:source_gen/source_gen.dart';

List<String> supportedClasses(LibraryReader library) {
  final annotatedElements =
      library.annotatedWith(TypeChecker.fromRuntime(SerializersFor)).toList();
  if (annotatedElements.isNotEmpty) {
    final annotation = _serializersForAnnotation(library.element);
    if (annotation.isNotEmpty) {
      final classes = _serializeForClasses(annotation);
      return classes.values.first as List<String>;
    }
  }
  return [];
}

Map<String, ElementAnnotation> _serializersForAnnotation(
    LibraryElement element) {
  final result = <String, ElementAnnotation>{};
  var accessors = element.definingCompilationUnit.accessors
      .where((element) =>
          element.isGetter && _getName(element.returnType) == 'Serializers')
      .toList();

  for (var accessor in accessors) {
    final annotations = accessor.variable.metadata
        .where((annotation) =>
            _getName(annotation.computeConstantValue()?.type) ==
            'SerializersFor')
        .toList();
    if (annotations.isEmpty) continue;

    result[accessor.name] = annotations.single;
  }

  return result;
}

String? _getName(DartType? dartType) {
  if (dartType == null) {
    return null;
  } else if (dartType is DynamicType) {
    return 'dynamic';
  } else if (dartType is FunctionType) {
    return _getName(dartType.returnType)! +
        ' Function(' +
        dartType.parameters.map((p) => _getName(p.type)).join(', ') +
        ')';
  } else if (dartType is InterfaceType) {
    var typeArguments = dartType.typeArguments;
    if (typeArguments.isEmpty || typeArguments.every((t) => t is DynamicType)) {
      return dartType.element.name;
    } else {
      final typeArgumentsStr = typeArguments.map(_getName).join(', ');
      return '${dartType.element.name}<$typeArgumentsStr>';
    }
  } else if (dartType is TypeParameterType) {
    return dartType.element.name;
  } else if (dartType is VoidType) {
    return 'void';
  } else {
    throw UnimplementedError('(${dartType.runtimeType}) $dartType');
  }
}

Map<String, List<dynamic>> _serializeForClasses(
    Map<String, ElementAnnotation> serializersForAnnotations) {
  var result = <String, List<String>>{};

  for (var field in serializersForAnnotations.keys) {
    final serializersForAnnotation = serializersForAnnotations[field]!;

    final types = serializersForAnnotation
        .computeConstantValue()!
        .getField('types')!
        .toListValue()!
        .toList()
        .map((dartType) => dartType.toTypeValue())
        .map((dartType) => dartType!.element as ClassElement?)
        .map((classElement) => classElement!.name)
        .toList();

    result.putIfAbsent(field, () => types);
  }
  return result;
}
