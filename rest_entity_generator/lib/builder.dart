library rest_entity_generator.builder;

import 'package:build/build.dart';
import 'package:rest_entity_generator/src/rest_entity_serializers_generator.dart';
import 'package:source_gen/source_gen.dart';

import 'src/rest_entity_generator.dart';

Builder restEntityBuilder(BuilderOptions options) =>
    SharedPartBuilder([RestEntityGenerator()], 'rest_entity');

Builder restEntitySerializersBuilder(BuilderOptions options) =>
    SharedPartBuilder(
        [RestEntitySerializerGenerator()], 'rest_entity_serializers');
